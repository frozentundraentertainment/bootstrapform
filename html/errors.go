package html

import (
	"errors"
)

// ErrOpen is returned when a tag is attempted to be opened and already is
var ErrOpen = errors.New("tag is already open")

// ErrClosed is returned when a tag is closed and you attempt to append to it
var ErrClosed = errors.New("tag is already closed")

// ErrNotOpen is returned when a tag is attempted to be closed but isn't open
var ErrNotOpen = errors.New("tag is not open")
