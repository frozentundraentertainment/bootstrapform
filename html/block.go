package html

import (
	"fmt"
	"strings"
)

// DoNotCloseTags is a list of tags that do not require a </tag>
var DoNotCloseTags = []string{
	"input",
}

// New returns a new HTML structure
func New(sb *strings.Builder, tag string, indentLevel int, indentChar *string) (*Block, *Tag) {
	node := new(Block)
	node.Builder = new(strings.Builder)
	node.Parent = sb
	node.IndentLevel = indentLevel
	node.IndentChar = indentChar
	return node, node.Open(tag)
}

// Block is a HTML code block builder
// TODO: Allow for indentation
type Block struct {
	Parent *strings.Builder
	*strings.Builder
	Error       error
	Tag         string
	OpenTag     Tag
	Block       []*Block
	RawHTML     customHTML
	IndentLevel int
	IndentChar  *string
}

func (b *Block) Open(tag string) *Tag {
	if b.Tag != "" {
		b.Error = ErrOpen
	}
	b.Tag = tag

	return b.OpenTag.open(b.Builder, tag, &b.IndentLevel, b.IndentChar)
}

func (b *Block) AddBlock(tag string) (*Block, *Tag) {
	newBlock, newTag := New(b.Builder, tag, b.IndentLevel+1, b.IndentChar)
	b.Block = append(b.Block, newBlock)
	return newBlock, newTag
}

func (b *Block) Render() (string, error) {
	if b.Error != nil {
		return "", b.Error
	}
	if b.Tag == "" {
		return "", ErrNotOpen
	}

	if b.RawHTML.BeforeOpenTag != "" {
		_, _ = b.WriteString(b.RawHTML.BeforeOpenTag)
	}

	if name, err := b.OpenTag.close(); err != nil {
		return name, err
	}

	if b.RawHTML.AfterOpenTag != "" {
		_, _ = b.WriteString(b.RawHTML.AfterOpenTag)
	}

	for _, node := range b.Block {
		_, _ = b.WriteString("\n")
		if name, err := node.Render(); err != nil {
			return name, err
		}
		// _, _ = b.WriteString("\n")
	}

	if b.RawHTML.BeforeEndTag != "" {
		_, _ = b.WriteString(b.RawHTML.BeforeEndTag)
	}

	closeTag := true
	for _, dncTag := range DoNotCloseTags {
		if dncTag == b.Tag {
			closeTag = false
		}
	}
	if closeTag {
		if len(b.Block) > 0 {
			_, _ = b.WriteString("\n")
			for i := 1; i <= b.IndentLevel; i++ {
				_, _ = b.WriteString(*b.IndentChar)
			}
		}
		_, _ = b.WriteString(fmt.Sprintf("</%s>", b.Tag))
	}

	if b.RawHTML.AfterEndTag != "" {
		_, _ = b.WriteString(b.RawHTML.AfterEndTag)
	}

	b.Parent.WriteString(b.String())

	return "", nil
}
