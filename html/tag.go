package html

import (
	"fmt"
	"strings"
)

// Tag is an HTML tag builder
type Tag struct {
	parent *strings.Builder
	*strings.Builder
	tag         string
	Error       error
	isOpen      bool
	isClosed    bool
	IndentLevel *int
	IndentChar  *string
}

func (t *Tag) open(sb *strings.Builder, tag string, indentLevel *int, indentChar *string) *Tag {
	if t.Error != nil {
		return t
	}
	if t.isOpen {
		t.Error = ErrOpen
		return t
	}
	if t.isClosed {
		t.Error = ErrClosed
		return t
	}

	t.parent = sb
	t.Builder = new(strings.Builder)
	t.isOpen = true
	t.tag = tag
	t.IndentLevel = indentLevel
	t.IndentChar = indentChar
	for i := 1; i <= *t.IndentLevel; i++ {
		_, _ = t.WriteString(*t.IndentChar)
	}
	_, _ = t.WriteString("<" + t.tag)
	return t
}

func (t *Tag) close() (string, error) {
	if t.Error != nil {
		return t.tag, t.Error
	}
	if t.isClosed {
		return t.tag, ErrClosed
	}
	if !t.isOpen {
		return t.tag, ErrNotOpen
	}

	t.isClosed = true
	t.isOpen = false
	_, _ = t.WriteString(">")
	t.parent.WriteString(t.String())
	return t.tag, nil
}

// AddAttributeWithData will only write the attribute if the string
// representation of the value is not empty
func (t *Tag) AddAttributeWithData(attribute string, v interface{}) *Tag {
	if value := fmt.Sprintf("%v", v); value != "" {
		_, _ = t.WriteString(fmt.Sprintf(" %s=%q", attribute, value))
	}
	return t
}

// AddAttribute will write only the attribute without data
func (t *Tag) AddAttribute(attribute string) *Tag {
	_, _ = t.WriteString(" " + attribute)
	return t
}

func (t *Tag) AddBoolean(attribute string, v bool) *Tag {
	_, _ = t.WriteString(fmt.Sprintf(" %s=%v", attribute, v))
	return t
}
