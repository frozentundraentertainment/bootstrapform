package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

func newLegend(text string) legend {
	var legend legend
	legend.Text = text
	legend.SpellCheck = true
	legend.Wrap = newWrapper()

	return legend
}

type legend struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	OverwriteCSS bool
	Text         string
	Wrap         wrapper
}

func (l *legend) Render(block *html.Block) {
	legend, tag := l.Wrap.Render(block).AddBlock("legend")
	legend.RawHTML.AfterOpenTag = l.Text

	if !l.OverwriteCSS {
	}

	// Render Global attributes
	l.Attributes.Render(tag)
	l.Events.Render(tag)
}
