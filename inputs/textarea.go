package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputCheckBox -	<input> elements of type checkbox are rendered by default as
// 					square boxes that are checked (ticked) when activated, like
// 					you might see in an official government paper form. They
// 					allow you to select single values for submission in a form
// 					(or not).
const InputTextArea InputType = "textarea"

// CheckBox
//
//
// Checked		Boolean; if present, the checkbox is toggled on by default
type TextArea struct {
	inputBase
	Rows int
	Cols int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *TextArea) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		i.ClassAdd("form-control")
	}

	// Then add the input
	_, inputTag := i.inputBase.Render(block)
	i.Type = InputTextArea

	if i.Rows > 0 {
		inputTag.AddAttributeWithData("rows", i.Rows)
	}
	if i.Cols > 0 {
		inputTag.AddAttributeWithData("cols", i.Cols)
	}
}
