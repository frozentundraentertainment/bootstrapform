package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

func newInputGroup() inputGroup {
	var ig inputGroup
	ig.SpellCheck = true
	ig.Wrap = newWrapper()

	return ig
}

type inputGroup struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	OverwriteCSS bool
	prepend      *Block
	append       *Block
	Wrap         wrapper
}

func (i *inputGroup) Prepend(outerTag string, innerTag string, text string) (*Block, *Block) {
	i.prepend = newBlock(outerTag, "")
	i.prepend.ClassAdd("input-group-prepend")

	textBlock := i.prepend.Inputs.AddBlock(innerTag, text)
	textBlock.ClassAdd("input-group-text")

	return i.prepend, textBlock
}

func (i *inputGroup) Append(outerTag string, innerTag string, text string) (*Block, *Block) {
	i.append = newBlock(outerTag, "")
	i.append.ClassAdd("input-group-append")

	textBlock := i.append.Inputs.AddBlock(innerTag, text)
	textBlock.ClassAdd("input-group-text")

	return i.append, textBlock
}

func (i *inputGroup) Render(htmlBlock *html.Block, input *inputBase) (*html.Block, *html.Tag) {
	// If not set, then just render the input
	if i.prepend == nil && i.append == nil {
		return input.renderInput(htmlBlock)
	}

	block, div := htmlBlock.AddBlock("div")

	if !i.OverwriteCSS {
		i.ClassAdd("input-group")
	}

	// Render Global attributes
	i.Attributes.Render(div)
	i.Events.Render(div)

	// Add form group name
	// div.AddAttributeWithData("id", i.ID)

	if i.prepend != nil {
		i.prepend.Render(block)
	}

	inputBlock, inputTag := input.renderInput(block)

	if i.append != nil {
		i.append.Render(block)
	}

	return inputBlock, inputTag
}
