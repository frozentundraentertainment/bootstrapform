package inputs

import (
	"fmt"
	"time"

	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputDate - 	HTML5 Only <input> elements of type date create input fields that
// 				let the user enter a date, either using a text box that
// 				automatically validates the content, or using a special date
// 				picker interface. The resulting value includes the year, month,
// 				and day, but not the time. The time and datetime-local input
// 				types support time and date/time inputs.
const InputDate InputType = "date"

// Date
//
// HTML5 Only
//
//
// max		The latest date to accept Format: (yyyy-mm-dd)
//
// min		The earliest date to accept Format: (yyyy-mm-dd)
//
// step		The stepping interval to use, such as when clicking up and down
// 			spinner buttons, as well as for validation
//
// pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}
type Date struct {
	inputBase
	max  string
	min  string
	Step int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Date) Render(block *html.Block) {
	i.inputBase.Render(block)
}

// SetMaxDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Date) SetMaxDateFromTime(date time.Time) {
	i.max = date.Format("2006-01-02")
}

// SetMaxDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Date) SetMaxDateFromString(day int, month int, year int) {
	i.max = fmt.Sprintf("%d-%d-%d", year, month, day)
}

// SetMinDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Date) SetMinDateFromTime(date time.Time) {
	i.min = date.Format("2006-01-02")
}

// SetMinDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Date) SetMinDateFromString(day int, month int, year int) {
	i.min = fmt.Sprintf("%d-%d-%d", year, month, day)
}
