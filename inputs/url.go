package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputURL -	HTML5 Only <input> elements of type url are used to let the user
// 				enter and edit a URL.
const InputURL InputType = "url"

// URL
//
//
// HTML5 Only
//
// maxlength	The maximum length the value may be, in UTF-16 characters
//
// minlength	The minimum length in characters that will be considered valid
//
// size			The number of characters wide the input field should be
type URL struct {
	inputBase
	MaxLength int
	MinLength int
	Size      int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *URL) Render(block *html.Block) {
	i.inputBase.Render(block)
}
