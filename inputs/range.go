package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputRange -	HTML5 Only <input> elements of type range let the user specify a
// 				numeric value which must be no less than a given value, and no
// 				more than another given value. The precise value, however, is not
// 				considered important. This is typically represented using a
// 				slider or dial control rather than a text entry box like the
// 				number input type. Because this kind of widget is imprecise, it
// 				shouldn't typically be used unless the control's exact value
// 				isn't important.
const InputRange InputType = "range"

// Range
//
//
// HTML5 Only
//
// max	The maximum value to accept for this input
//
// min	The minimum value to accept for this input
//
// step	A stepping interval to use when using up and down arrows to adjust the
// 		value, as well as for validation
type Range struct {
	inputBase
	Max  int
	Min  int
	Step float32
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Range) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		// 	i.ClassAdd("form-control-range")
	}

	// Then add the input
	_, inputTag := i.inputBase.Render(block)
	if i.Min > 0 {
		inputTag.AddAttributeWithData("min", i.Min)
	}
	if i.Max > 0 {
		inputTag.AddAttributeWithData("max", i.Max)
	}
	if i.Step > 0 {
		inputTag.AddAttributeWithData("step", i.Step)
	}
}
