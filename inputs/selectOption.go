package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// SelectOption
//
//
// Checked		Boolean; if present, the checkbox is toggled on by default
type SelectOption struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	Value string
	Wrap  wrapper
	Text  string
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *SelectOption) Render(block *html.Block) {

	optionBlock, optionTag := i.Wrap.Render(block).AddBlock("option")
	// Render Global attributes
	i.Attributes.Render(optionTag)
	i.Events.Render(optionTag)

	optionTag.AddAttributeWithData("value", i.Value)
	optionBlock.RawHTML.AfterOpenTag = i.Text
}
