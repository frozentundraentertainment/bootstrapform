package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputColor -	HTML5 Only <input> elements of type "color" provide a user
// 				interface element that lets a user specify a color, either by
// 				using a visual color picker interface or by entering the color
// 				into a text field in "#rrggbb" hexadecimal format. Only simple
// 				colors (with no alpha channel) are allowed. The values are
// 				compatible with CSS.
const InputColor InputType = "color"

// Color
//
// HTML5 Only
type Color struct {
	inputBase
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Color) Render(block *html.Block) {
	i.inputBase.Render(block)
}
