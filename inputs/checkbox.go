package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputCheckBox -	<input> elements of type checkbox are rendered by default as
// 					square boxes that are checked (ticked) when activated, like
// 					you might see in an official government paper form. They
// 					allow you to select single values for submission in a form
// 					(or not).
const InputCheckBox InputType = "checkbox"

// CheckBox
//
//
// Checked		Boolean; if present, the checkbox is toggled on by default
type CheckBox struct {
	inputBase
	Checked bool
	Inline  bool
}

// Per Bootstrap
// <div class="form-check">  // Add form-check-inline for a horizontal layout
//  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
//  <label class="form-check-label" for="defaultCheck1">
//    Default checkbox
//  </label>
// </div>

// Render will add the appropriate input HTML tags to the HTML block
func (i *CheckBox) Render(block *html.Block) {

	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		if i.Inline {
			if i.BootstrapCustom {
				i.FormGroup.ClassAdd("custom-control-inline")
			} else {
				i.FormGroup.ClassAdd("form-check-inline")
			}
		}
	}

	// Set render order
	if i.Order == OrderDefault {
		i.Order = OrderInputLabelHint
	}

	// Render the input and apply specific attributes
	_, inputTag := i.inputBase.Render(block)
	if i.Checked {
		inputTag.AddAttribute("checked")
	}
}
