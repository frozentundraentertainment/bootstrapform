package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
	"gitlab.com/frozentundraentertainment/bootstrapform/types"
)

// InputImage -	<input> elements of type image are used to create graphical
// 				submit buttons, i.e. submit buttons that take the form of an
// 				image rather than text.
const InputImage InputType = "image"

// Image
//
//
// alt				Alternate string to display when the image can't be shown
//
// formaction		The URL to which to submit the data
//
// formenctype		The encoding method to use when submitting the form data
//
// formmethod		The HTTP method to use when submitting the form
//
// formnovalidate	A Boolean which, if present, indicates that the form
// 					shouldn't be validated before submission
//
// formtarget		A string indicating a browsing context from where to load the
// 					results of submitting the form
//
// height			The height, in CSS pixels, at which to draw the image
//
// src				The URL from which to load the image
//
// width			The width, in CSS pixels, at which to draw the image
type Image struct {
	inputBase
	alt         string
	FormAction  string
	FormEncType types.Encoding
	FormMethod  types.HttpMethod
	Target      types.Target
	iFrameName  string
	height      int
	src         string
	width       int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Image) Render(block *html.Block) {
	i.inputBase.Render(block)
}

// TargetIFrame will specify an iFrame as the target of the form
func (i *Image) TargetIFrame(name string) *Image {
	i.iFrameName = name
	i.Target = types.TargetIFrame
	return i
}
