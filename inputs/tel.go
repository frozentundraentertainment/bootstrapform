package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputTel -	HTML5 Only <input> elements of type tel are used to let the user
// 				enter and edit a telephone number. Unlike <input type="email">
// 				and <input type="url"> , the input value is not automatically
// 				validated to a particular format before the form can be submitted,
// 				because formats for telephone numbers vary so much around the
// 				world.
const InputTel InputType = "tel"

// Tel
//
//
// HTML5 Only
//
// maxlength	The maximum length the value may be, in UTF-16 characters
//
// minlength	The minimum length in characters that will be considered valid
//
// size			The number of characters wide the input field should be
type Tel struct {
	inputBase
	MaxLength int
	MinLength int
	Size      int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Tel) Render(block *html.Block) {
	i.inputBase.Render(block)
}
