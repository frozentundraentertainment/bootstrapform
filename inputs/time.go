package inputs

import (
	"fmt"
	"time"

	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputTime -	HTML5 Only <input> elements of type time create input fields
// 				designed to let the user easily enter a time (hours and minutes,
// 				and optionally seconds).
//
// 				The control's user interface will vary from browser to browser.
// 				Support is good in modern browsers, with Safari being the sole
// 				major browser not yet implementing it; in Safari, and any other
// 				browsers that don't support <time>, it degrades gracefully to
// 				<input type="text">.
const InputTime InputType = "time"

// Time
//
// HTML5 Only
//
//
// max		The latest date to accept Format: (yyyy-mm-ddThh:mm)
//
// min		The earliest date to accept Format: (yyyy-mm-ddThh:mm)
//
// step		The stepping interval to use, such as when clicking up and down
// 			spinner buttons, as well as for validation
//
// pattern="[0-9]{2}:[0-9]{2}"
type Time struct {
	inputBase
	max  string
	min  string
	Step int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Time) Render(block *html.Block) {
	i.inputBase.Render(block)
}

// SetMaxDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Time) SetMaxTimeFromTime(date time.Time) {
	i.max = date.Format("15:04")
}

// SetMaxDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Time) SetMaxDateFromString(hour int, minute int) {
	i.max = fmt.Sprintf("%d:%d", hour, minute)
}

// SetMinDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Time) SetMinDateFromTime(date time.Time) {
	i.min = date.Format("15:04")
}

// SetMinDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Time) SetMinDateFromString(hour int, minute int) {
	i.min = fmt.Sprintf("%d:%d", hour, minute)
}
