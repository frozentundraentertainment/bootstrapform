package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

type wrapperTag string

const WrapDiv wrapperTag = "div"
const WrapSpan wrapperTag = "Span"
const WrapNone wrapperTag = ""

func newWrapper() wrapper {
	var wrap wrapper
	wrap.SpellCheck = true

	return wrap
}

type wrapper struct {
	Type wrapperTag
	HTMLGlobals.Attributes
	HTMLGlobals.Events
}

func (w *wrapper) AddClass(class string) {
	if w.Type == WrapNone {
		w.Type = WrapDiv
	}
	w.Attributes.ClassAdd(class)
}

func (w *wrapper) AddStyle(style string, value string) {
	if w.Type == WrapNone {
		w.Type = WrapDiv
	}
	w.Attributes.StyleAdd(style, value)
}

func (w *wrapper) Render(inputGroup *html.Block) *html.Block {
	// If no wrapper set, then return the parent HTML Block
	if w.Type == WrapNone {
		return inputGroup
	}
	// Else create the specified wrapper

	// Create the wrapping tag on the form-group
	block, tag := inputGroup.AddBlock(string(w.Type))

	// Render Global attributes
	w.Attributes.Render(tag)
	w.Events.Render(tag)

	return block
}
