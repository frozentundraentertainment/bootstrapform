package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

func NewFormGroup() FormGroup {
	var fg FormGroup
	fg.SpellCheck = true
	fg.Wrap = newWrapper()

	return fg
}

type FormGroup struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	OverwriteCSS bool
	Enable       bool
	Wrap         wrapper
}

func (f *FormGroup) ClassAdd(class string) {
	f.Attributes.ClassAdd(class)
	f.Enable = true
}

func (f *FormGroup) StyleAdd(style string, value string) {
	f.Attributes.StyleAdd(style, value)
	f.Enable = true
}

func (f *FormGroup) Render(htmlBlock *html.Block) *html.Block {
	if !f.Enable {
		return htmlBlock
	}

	block, div := f.Wrap.Render(htmlBlock).AddBlock("div")

	if !f.OverwriteCSS {
		// f.ClassAdd("form-group")
	}

	// Render Global attributes
	f.Attributes.Render(div)
	f.Events.Render(div)

	// Add form group name
	// div.AddAttributeWithData("id", i.ID)

	return block
}
