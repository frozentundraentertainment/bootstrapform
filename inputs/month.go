package inputs

import (
	"fmt"
	"time"

	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputMonth -	HTML5 Only <input> elements of type month create input fields
// 				that let the user enter a month and year allowing a month and
// 				year to be easily entered. The value is a string whose value is
// 				in the format "YYYY-MM", where YYYY is the four-digit year and
// 				MM is the month number.
const InputMonth InputType = "month"

// Month
//
//
// HTML5 Only
//
// max	The latest year and month to accept as a valid input
//
// min	The earliest year and month to accept as a valid input
//
// step	A stepping interval to use when incrementing and decrementing the value
// 		of the input field
type Month struct {
	inputBase
	max  string
	min  string
	Step int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Month) Render(block *html.Block) {
	i.inputBase.Render(block)
}

// SetMaxDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Month) SetMaxDateFromTime(date time.Time) {
	i.max = date.Format("2006-01")
}

// SetMaxDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Month) SetMaxDateFromString(month int, year int) {
	i.max = fmt.Sprintf("%d-%d", year, month)
}

// SetMinDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Month) SetMinDateFromTime(date time.Time) {
	i.min = date.Format("2006-01")
}

// SetMinDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Month) SetMinDateFromString(month int, year int) {
	i.min = fmt.Sprintf("%d-%d", year, month)
}
