package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputEmail -	HTML5 Only <input> elements of type email are used to let the
// 				user enter and edit an e-mail address, or, if the multiple
// 				attribute is specified, a list of e-mail addresses.
const InputEmail InputType = "email"

// Email
//
// HTML5 Only
//
//
// maxlength	The maximum number of characters the input should accept
//
// minlength	The minimum number of characters long the input can be and still
// 				be considered valid
//
// multiple		Whether or not to allow multiple, comma-separated, e-mail
// 				addresses to be entered
//
// readonly		A Boolean attribute indicating whether or not the contents of the
// 				input should be read-only
//
// size			A number indicating how many characters wide the input field
// 				should be
//
// spellcheck	Controls whether or not to enable spell checking for the input
// 				field, or if the default spell checking configuration should be
// 				used
type Email struct {
	inputBase
	MaxLength int
	MinLength int
	Multiple  bool
	Size      int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Email) Render(block *html.Block) {

	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		i.ClassAdd("form-control")
	}

	// Then add the input
	_, inputTag := i.inputBase.Render(block)
	if i.MinLength > 0 {
		inputTag.AddAttributeWithData("minlength", i.MinLength)
	}
	if i.MaxLength > 0 {
		inputTag.AddAttributeWithData("maxlength", i.MaxLength)
	}
	if i.Size > 0 {
		inputTag.AddAttributeWithData("size", i.Size)
	}
}
