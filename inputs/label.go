package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

func newLabel(text string) label {
	var label label
	label.Text = text
	label.SpellCheck = true
	label.Wrap = newWrapper()

	return label
}

type label struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	OverwriteCSS bool
	Text         string
	Wrap         wrapper
	preBlock     *Block
	postBlock    *Block
}

// AddPreBlock will add a HTML block that will be rendered before the input
func (l *label) AddPreBlock(tag string, text string) *Block {
	l.preBlock = newBlock(tag, text)
	return l.preBlock
}

// AddPostBlock will add a HTML block that will be rendered before the input
func (l *label) AddPostBlock(tag string, text string) *Block {
	l.postBlock = newBlock(tag, text)
	return l.postBlock
}

func (l *label) Render(block *html.Block, labelFor string) {
	if l.Text == "" {
		return
	}

	// Render the Pre Input HTML Block
	if l.preBlock != nil {
		l.preBlock.Render(block)
	}

	label, tag := block.AddBlock("label")
	label.RawHTML.AfterOpenTag = l.Text
	tag.AddAttributeWithData("for", labelFor)

	if !l.OverwriteCSS {
	}

	// Render Global attributes
	l.Attributes.Render(tag)
	l.Events.Render(tag)

	// Render the Post Input HTML Block
	if l.postBlock != nil {
		l.postBlock.Render(block)
	}
}
