package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

func newHint(text string) hint {
	var hint hint
	hint.Text = text
	hint.SpellCheck = true
	hint.Wrap = newWrapper()

	return hint
}

type hint struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	OverwriteCSS bool
	Text         string
	Wrap         wrapper
}

func (h *hint) Render(block *html.Block) {
	if h.Text == "" {
		return
	}
	hint, tag := block.AddBlock("small")
	hint.RawHTML.AfterOpenTag = h.Text

	if !h.OverwriteCSS {
		h.ClassAdd("form-text")
		h.ClassAdd("text-muted")
	}

	// Render Global attributes
	h.Attributes.Render(tag)
	h.Events.Render(tag)
}
