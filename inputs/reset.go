package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputReset -	<input> elements of type "reset"  are rendered as buttons, with
// 				a default click event handler that resets all of the inputs in
// 				the form to their initial values.
const InputReset InputType = "reset"

// Reset
//
//
type Reset struct {
	inputBase
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Reset) Render(block *html.Block) {
	i.inputBase.Render(block)
}
