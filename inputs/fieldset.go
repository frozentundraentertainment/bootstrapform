package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputFieldSet - group related elements in a form
const InputFieldSet InputType = "fieldset"

// FieldSet
//
//
// legend		Similar to a label, displayed above the group of inputs
type FieldSet struct {
	inputBase
	Legend    legend
	InputWrap wrapper
	Inputs    InputArray
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *FieldSet) Render(block *html.Block) {
	fieldSet, _ := i.inputBase.Render(block)

	if !i.OverwriteCSS {
	}

	// Render the optional input wrapper
	iWrap := i.InputWrap.Render(fieldSet)

	// Render the Legend
	if i.Legend.Text != "" {
		i.Legend.Render(iWrap)
	}

	// Render Inputs
	i.Inputs.Render(iWrap)
	// for _, input := range i.Inputs.inputs {
	// 	input.Render(iWrap)
	// }
}
