package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputSearch -	HTML5 Only <input> elements of type search are text fields
// 					designed for the user to enter search queries into. These are
// 					functionally identical to text inputs, but may be styled
// 					differently by the user agent.
const InputSearch InputType = "search"

// Search
//
//
// HTML5 Only
//
// maxlength	The maximum length the value may be, in UTF-16 characters
//
// minlength	The minimum length in characters that will be considered valid
//
// size			The number of characters wide the input field should be
type Search struct {
	inputBase
	MaxLength int
	MinLength int
	Size      int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Search) Render(block *html.Block) {
	i.inputBase.Render(block)
}
