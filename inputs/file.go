package inputs

import (
	"strings"

	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputFile -	<input> elements with type="file" let the user choose one or more
// 				files from their device storage. Once chosen, the files can be
// 				uploaded to a server using form submission, or manipulated using
// 				JavaScript code and the File API.
const InputFile InputType = "file"

// File
//
//
// accept	One or more unique file type specifiers describing file types to allow
// capture	What source to use for capturing image or video data
// files	A FileList listing the chosen files
// multiple	A Boolean which, if present, indicates that the user may choose more
// 			than one file
type File struct {
	inputBase
	AcceptMimeTypes []string
	Capture         string
	FileList        string
	Multiple        bool
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *File) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		// 	i.ClassAdd("form-control-file")
	}

	// Then add the input
	_, inputTag := i.inputBase.Render(block)
	if i.Multiple {
		inputTag.AddAttribute("multiple")
	}
	inputTag.AddAttributeWithData("filelist", i.FileList)
	inputTag.AddAttributeWithData("capture", i.Capture)
	if len(i.AcceptMimeTypes) > 0 {
		inputTag.AddAttributeWithData("accept", strings.Join(i.AcceptMimeTypes, "|"))
	}
}
