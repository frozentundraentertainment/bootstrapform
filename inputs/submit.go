package inputs

import (
	"strings"

	"gitlab.com/frozentundraentertainment/bootstrapform/html"
	"gitlab.com/frozentundraentertainment/bootstrapform/types"
)

// InputSubmit -	<input> elements of type submit are rendered as buttons.
// 					When the click event occurs (typically because the user
// 					clicked the button), the user agent attempts to submit the
// 					form to the server.
const InputSubmit InputType = "submit"

// Submit
//
//
// formaction		The URL to which to submit the form's data; overrides the
// 					form's action attribute, if any
//
// formenctype		A string specifying the encoding type to use for the form data
//
// formmethod		The HTTP method (get or post) to use when submitting the form.
//
// formnovalidate	A Boolean which, if present, means the form's fields will not
// 					be subjected to constraint validation before submitting the
// 					data to the server
//
// formtarget		The browsing context into which to load the response returned
// 					by the server after submitting the form
type Submit struct {
	inputBase
	FormAction     string
	FormEncoding   types.Encoding
	FormMethod     types.HttpMethod
	FormNoValidate bool
	FormTarget     types.Target
	iFrameName     string
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Submit) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		i.ClassAdd("form-control")
	}

	// Then add the input
	_, inputTag := i.inputBase.Render(block)
	inputTag.AddAttributeWithData("formaction", i.FormAction)

	if i.FormMethod != "" {
		if i.FormMethod != types.MethodGet {
			// Todo: create hidden input with method type
			inputTag.AddAttributeWithData("formmethod", strings.ToLower(string(types.MethodPost)))
			inputTag.AddAttributeWithData("formencoding", i.FormEncoding)
		} else {
			inputTag.AddAttributeWithData("method", strings.ToLower(string(types.MethodGet)))
		}
	}
	if i.FormNoValidate {
		inputTag.AddAttribute("formnovalidate")
	}
	if i.FormTarget == types.TargetIFrame {
		i.FormTarget = types.Target(i.iFrameName)
	}
	inputTag.AddAttributeWithData("formtarget", i.FormTarget)
}

// TargetIFrame will specify an iFrame as the target of the form
func (i *Submit) TargetIFrame(name string) *Submit {
	i.iFrameName = name
	i.FormTarget = types.TargetIFrame
	return i
}
