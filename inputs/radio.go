package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputRadio -	<input> elements of type radio are generally used in radio groups
// 				—collections of radio buttons describing a set of related
// 				options. Only one radio button in a given group can be selected
// 				at the same time. Radio buttons are typically rendered as small
// 				circles, which are filled or highlighted when selected.
const InputRadio InputType = "radio"

// Radio
//
//
// Checked		A Boolean indicating whether or not this radio button is the
// 				currently-selected item in the group
type Radio struct {
	inputBase
	Checked bool
	Inline  bool
}

// Per Bootstrap
// <div class="form-check">  // Add form-check-inline for a horizontal layout
//  <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
//  <label class="form-check-label" for="exampleRadios1">
//    Default radio
//  </label>
// </div>

// Render will add the appropriate input HTML tags to the HTML block
func (i *Radio) Render(block *html.Block) {

	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		// i.ClassAdd("form-check-input")
		// i.Label.ClassAdd("form-check-label")
		if i.Inline {
			if i.BootstrapCustom {
				i.FormGroup.ClassAdd("custom-control-inline")
			} else {
				i.FormGroup.ClassAdd("form-check-inline")
			}
		}
	}

	// Set render order
	if i.Order == OrderDefault {
		i.Order = OrderInputLabelHint
	}

	// Render the input and apply specific attributes
	_, inputTag := i.inputBase.Render(block)
	if i.Checked {
		inputTag.AddAttribute("checked")
	}
}
