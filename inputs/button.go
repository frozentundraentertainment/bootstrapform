package inputs

import (
	"strings"

	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
	"gitlab.com/frozentundraentertainment/bootstrapform/types"
)

type buttonType string

// BtnSubmit - The button submits the form data to the server. This is the
// default if the attribute is not specified, or if the attribute is dynamically
// changed to an empty or invalid value.
const BtnSubmit buttonType = "submit"

// BtnReset - The button resets all the controls to their initial values.
const BtnReset buttonType = "reset"

// BtnButton - The button has no default behavior. It can have client-side
// scripts associated with the element's events, which are triggered when the
// events occur.
const BtnButton buttonType = "button"

// Attributes
//
//
// autofocus HTML5
//
// This Boolean attribute lets you specify that the button should have input
// focus when the page loads, unless the user overrides it, for example by typing
// in a different control. Only one form-associated element in a document can
// have this attribute specified.
//
//
// form HTML5
//
// The form element that the button is associated with (its form owner). The
// value of the attribute must be the id attribute of a <form> element in the
// same document. If this attribute is not specified, the <button> element will
// be associated to an ancestor <form> element, if one exists. This attribute
// enables you to associate <button> elements to <form> elements anywhere within
// a document, not just as descendants of <form> elements.
//
//
// formaction HTML5
//
// The URI of a program that processes the information submitted by the button.
// If specified, it overrides the action attribute of the button's form owner.
//
//
// formenctype HTML5
//
// If the button is a submit button, this attribute specifies the type of content
// that is used to submit the form to the server. Possible values are:
//
// application/x-www-form-urlencoded: The default value if the attribute is not
// specified.
//
// multipart/form-data: Use this value if you are using an <input> element with
// the type attribute set to file.
//
// text/plain
// If this attribute is specified, it overrides the enctype attribute of the
// button's form owner.
//
//
// formmethod HTML5
//
// If the button is a submit button, this attribute specifies the HTTP method
// that the browser uses to submit the form. Possible values are:
//
// post: The data from the form are included in the body of the form and sent to
// the server.
//
// get: The data from the form are appended to the form attribute URI, with a '?'
// as a separator, and the resulting URI is sent to the server. Use this method
// when the form has no side-effects and contains only ASCII characters.
//
// If specified, this attribute overrides the method attribute of the button's
// form owner.
//
//
// formnovalidate HTML5
//
// If the button is a submit button, this Boolean attribute specifies that the
// form is not to be validated when it is submitted. If this attribute is
// specified, it overrides the novalidate attribute of the button's form owner.
//
//
// formtarget HTML5
//
// If the button is a submit button, this attribute is a name or keyword
// indicating where to display the response that is received after submitting the
// form. This is a name of, or keyword for, a browsing context (for example, tab,
// window, or inline frame). If this attribute is specified, it overrides the
// target attribute of the button's form owner. The following keywords have
// special meanings:
//
// _self: Load the response into the same browsing context as the current one.
// This value is the default if the attribute is not specified.
//
// _blank: Load the response into a new unnamed browsing context.
//
// _parent: Load the response into the parent browsing context of the current one.
// If there is no parent, this option behaves the same way as _self.
//
// _top: Load the response into the top-level browsing context (that is, the
// browsing context that is an ancestor of the current one, and has no parent).
// If there is no parent, this option behaves the same way as _self.
//
//
// name
//
// The name of the button, which is submitted with the form data.
//
//
// type
//
// The type of the button. Possible values are:
//
// submit: The button submits the form data to the server. This is the default
// if the attribute is not specified, or if the attribute is dynamically changed
// to an empty or invalid value.
//
// reset: The button resets all the controls to their initial values.
//
// button: The button has no default behavior. It can have client-side scripts
// associated with the element's events, which are triggered when the events occur.
//
// value
//
// The initial value of the button. It defines the value associated with the
// button which is submitted with the form data. This value is passed to the
// server in params when the form is submitted.
type Button struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	OverwriteCSS   bool
	AutoFocus      bool
	AutoComplete   bool
	Disabled       bool
	FormAction     string
	FormEncType    types.Encoding
	FormMethod     types.HttpMethod
	FormNoValidate bool
	FormTarget     types.Target
	iFrameName     string
	Name           string
	Type           buttonType
	Value          string
	Text           string
	Wrap           wrapper
	FormGroup      FormGroup
}

// GenerateID is a stub to fit the Input interface
func (i *Button) GenerateID(num int) {
	_ = num
	return
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Button) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		i.ClassAdd("btn")
		i.ClassAdd("btn-primary")
	}

	// Create the input block
	btnGroup, btnTag := i.Wrap.Render(i.FormGroup.Render(block)).AddBlock("button")
	if i.AutoFocus {
		btnTag.AddAttribute("autofocus")
	}
	if !i.AutoComplete {
		btnTag.AddAttributeWithData("autocomplete", "off")
	}
	if i.Disabled {
		btnTag.AddAttribute("disabled")
	}

	btnTag.AddAttributeWithData("formaction", i.FormAction)
	if i.FormMethod != "" {
		if i.FormMethod != types.MethodGet {
			// Todo: create hidden input with method type
			btnTag.AddAttributeWithData("formmethod", strings.ToLower(string(types.MethodPost)))
			btnTag.AddAttributeWithData("formencoding", i.FormEncType)
		} else {
			btnTag.AddAttributeWithData("method", strings.ToLower(string(types.MethodGet)))
		}
	}
	if i.FormNoValidate {
		btnTag.AddAttribute("formnovalidate")
	}
	if i.FormTarget == types.TargetIFrame {
		i.FormTarget = types.Target(i.iFrameName)
	}
	btnTag.AddAttributeWithData("formtarget", i.FormTarget)

	btnTag.AddAttributeWithData("name", i.Name)
	btnTag.AddAttributeWithData("type", i.Type)
	btnTag.AddAttributeWithData("value", i.Value)

	i.Attributes.Render(btnTag)
	i.Events.Render(btnTag)

	btnGroup.RawHTML.AfterOpenTag = i.Text
}

// TargetIFrame will specify an iFrame as the target of the form
func (i *Button) TargetIFrame(name string) *Button {
	i.iFrameName = name
	i.FormTarget = types.TargetIFrame
	return i
}
