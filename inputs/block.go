package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

func newBlock(tag string, text string) *Block {
	input := new(Block)
	input.Tag = tag
	input.SpellCheck = true
	input.Inputs.Wrap = newWrapper()
	input.Text = text

	return input
}

type Block struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	Tag    string
	Text   string
	Inputs InputArray
}

// GenerateID is a stub to fit the Input interface
func (b *Block) GenerateID(num int) {
	return
}

func (b *Block) Render(block *html.Block) {
	subBlock, tag := block.AddBlock(b.Tag)
	subBlock.RawHTML.AfterOpenTag = b.Text

	// Render Global attributes
	b.Attributes.Render(tag)
	b.Events.Render(tag)

	// Render Inputs
	for _, input := range b.Inputs.inputs {
		input.Render(subBlock)
	}
}
