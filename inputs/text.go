package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputText -	<input> elements of type text create basic single-line text fields.
const InputText InputType = "text"

// Text
//
//
// maxlength	The maximum length the value may be, in UTF-16 characters
//
// minlength	The minimum length in characters that will be considered valid
//
// size			The number of characters wide the input field should be
type Text struct {
	inputBase
	MaxLength int
	MinLength int
	Size      int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Text) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		// 	i.ClassAdd("form-control")
	}

	// Then add the input
	_, inputTag := i.inputBase.Render(block)
	if i.MinLength > 0 {
		inputTag.AddAttributeWithData("minlength", i.MinLength)
	}
	if i.MaxLength > 0 {
		inputTag.AddAttributeWithData("maxlength", i.MaxLength)
	}
	if i.Size > 0 {
		inputTag.AddAttributeWithData("size", i.Size)
	}
}
