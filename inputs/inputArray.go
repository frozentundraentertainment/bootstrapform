package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

func NewInputGroup(formID *string, groupNum int) *InputArray {
	ig := new(InputArray)
	ig.FormID = formID
	ig.GroupNumber = groupNum
	ig.FormGroup = NewFormGroup()
	ig.Wrap = newWrapper()

	return ig
}

type InputArray struct {
	inputs      []Input
	FormGroup   FormGroup
	Wrap        wrapper
	FormID      *string
	GroupNumber int
}

func (i *InputArray) Render(htmlBlock *html.Block) {

	if !i.FormGroup.OverwriteCSS && i.FormGroup.Enable {
		i.FormGroup.ClassAdd("form-group")
	}

	inputBlock := i.Wrap.Render(i.FormGroup.Render(htmlBlock))

	// Render Inputs
	for _, input := range i.inputs {
		input.Render(inputBlock)
	}
}

// AddBlock will append a custom HTML block to the slice and return it for customization
func (i *InputArray) AddBlock(tag string, text string) *Block {
	input := newBlock(tag, text)
	i.inputs = append(i.inputs, input)
	return input
}

// AddCheckBox will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddCheckBox(name string, label string, hint string) *CheckBox {
	input := new(CheckBox)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-check")
	// input.EnableFGWithoutLabelHint = true
	input.Type = InputCheckBox
	i.inputs = append(i.inputs, input)
	return input
}

// AddColor will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddColor(name string, label string, hint string) *Color {
	input := new(Color)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputColor
	i.inputs = append(i.inputs, input)
	return input
}

// AddDate will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddDate(name string, label string, hint string) *Date {
	input := new(Date)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputDate
	i.inputs = append(i.inputs, input)
	return input
}

// AddDateTimeLocal will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddDateTimeLocal(name string, label string, hint string) *DateTimeLocal {
	input := new(DateTimeLocal)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputDateTimeLocal
	i.inputs = append(i.inputs, input)
	return input
}

// AddEmail will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddEmail(name string, label string, hint string) *Email {
	input := new(Email)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputEmail
	i.inputs = append(i.inputs, input)
	return input
}

// AddFieldSet will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddFieldSet(name string, legend string) *FieldSet {
	input := new(FieldSet)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, "", "")
	input.Legend = newLegend(legend)
	input.Inputs = *NewInputGroup(i.FormID, i.GroupNumber+100)
	input.InputWrap = newWrapper()
	input.Type = InputFieldSet
	i.inputs = append(i.inputs, input)
	return input
}

// AddFile will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddFile(name string, label string, hint string) *File {
	input := new(File)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputFile
	i.inputs = append(i.inputs, input)
	return input
}

// AddImage will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddImage(name string, label string, hint string) *Image {
	input := new(Image)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputImage
	i.inputs = append(i.inputs, input)
	return input
}

// AddMonth will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddMonth(name string, label string, hint string) *Month {
	input := new(Month)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputMonth
	i.inputs = append(i.inputs, input)
	return input
}

// AddNumber will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddNumber(name string, label string, hint string) *Number {
	input := new(Number)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputNumber
	i.inputs = append(i.inputs, input)
	return input
}

// AddPassword will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddPassword(name string, label string, hint string) *Password {
	input := new(Password)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputPassword
	i.inputs = append(i.inputs, input)
	return input
}

// AddRadio will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddRadio(name string, label string, hint string) *Radio {
	input := new(Radio)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-check")
	// input.EnableFGWithoutLabelHint = true
	input.Type = InputRadio
	i.inputs = append(i.inputs, input)
	return input
}

// AddRange will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddRange(name string, label string, hint string) *Range {
	input := new(Range)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputRange
	i.inputs = append(i.inputs, input)
	return input
}

// AddReset will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddReset(name string, label string, hint string) *Reset {
	input := new(Reset)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputReset
	i.inputs = append(i.inputs, input)
	return input
}

// AddSearch will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddSearch(name string, label string, hint string) *Search {
	input := new(Search)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputSearch
	i.inputs = append(i.inputs, input)
	return input
}

// AddSelect will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddSelect(name string, label string, hint string) *Select {
	input := new(Select)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputSelect
	i.inputs = append(i.inputs, input)
	return input
}

// AddSubmit will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddSubmit(name string, text string) *Button {
	input := new(Button)
	input.Type = BtnSubmit
	input.Text = text
	input.Name = name
	input.FormGroup = NewFormGroup()
	input.FormGroup.ClassAdd("form-group")
	input.FormGroup.Enable = false
	input.SpellCheck = true
	input.Wrap = newWrapper()
	input.AutoComplete = true
	i.inputs = append(i.inputs, input)
	return input
}

// AddTel will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddTel(name string, label string, hint string) *Tel {
	input := new(Tel)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputTel
	i.inputs = append(i.inputs, input)
	return input
}

// AddText will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddText(name string, label string, hint string) *Text {
	input := new(Text)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputText
	i.inputs = append(i.inputs, input)
	return input
}

// AddTextArea will append a textarea input to the slice and return it for customization
func (i *InputArray) AddTextArea(name string, label string, hint string) *TextArea {
	input := new(TextArea)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputTextArea
	i.inputs = append(i.inputs, input)
	return input
}

// AddTime will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddTime(name string, label string, hint string) *Time {
	input := new(Time)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputTime
	i.inputs = append(i.inputs, input)
	return input
}

// AddURL will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddURL(name string, label string, hint string) *URL {
	input := new(URL)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputURL
	i.inputs = append(i.inputs, input)
	return input
}

// AddWeek will append a checkbox input to the slice and return it for customization
func (i *InputArray) AddWeek(name string, label string, hint string) *Week {
	input := new(Week)
	input.init(i.FormID, len(i.inputs), i.GroupNumber, name, label, hint)
	input.FormGroup.Enable = true
	// input.FormGroup.ClassAdd("form-group")
	input.Type = InputWeek
	i.inputs = append(i.inputs, input)
	return input
}
