package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputPassword -	<input> elements of type password provide a way for the user
// 					to securely enter a password. The element is presented as a
// 					one-line plain text editor control in which the text is
// 					obscured so that it cannot be read, usually by replacing each
// 					character with a symbol such as the asterisk ("*") or a dot
// 					("•"). This character will vary depending on the user agent
// 					and OS.
const InputPassword InputType = "password"

// Password
//
//
// maxlength	The maximum length the value may be, in UTF-16 characters
//
// minlength	The minimum length in characters that will be considered valid
//
// size			The number of characters wide the input field should be
type Password struct {
	inputBase
	MaxLength int
	MinLength int
	Size      int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Password) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		i.ClassAdd("form-control")
	}

	// Then add the input
	_, inputTag := i.inputBase.Render(block)
	if i.MinLength > 0 {
		inputTag.AddAttributeWithData("minlength", i.MinLength)
	}
	if i.MaxLength > 0 {
		inputTag.AddAttributeWithData("maxlength", i.MaxLength)
	}
	if i.Size > 0 {
		inputTag.AddAttributeWithData("size", i.Size)
	}
}
