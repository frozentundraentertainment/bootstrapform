package inputs

import (
	"fmt"
	"time"

	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputWeek -	HTML5 Only <input> elements of type week create input fields
// 				allowing easy entry of a year plus the ISO 8601 week number
// 				during that year (i.e., week 1 to 52 or 53).
const InputWeek InputType = "week"

// Week
//
//
// HTML5 Only
//
// max	The latest year and month to accept as a valid input
//
// min	The earliest year and month to accept as a valid input
//
// step	A stepping interval to use when incrementing and decrementing the value
// 		of the input field
type Week struct {
	inputBase
	max  string
	min  string
	Step int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Week) Render(block *html.Block) {
	i.inputBase.Render(block)
}

// SetMaxDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Week) SetMaxDateFromTime(date time.Time) {
	year, week := date.ISOWeek()
	i.SetMaxDateFromString(year, week)
}

// SetMaxDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Week) SetMaxDateFromString(month int, year int) {
	i.max = fmt.Sprintf("%d-W%d", year, month)
}

// SetMinDateFromTime will convert a given time to the format needed for the
// form attribute
func (i *Week) SetMinDateFromTime(date time.Time) {
	year, week := date.ISOWeek()
	i.SetMinDateFromString(year, week)
}

// SetMinDateFromString will convert a given time to the format needed for the
// // form attribute
func (i *Week) SetMinDateFromString(month int, year int) {
	i.max = fmt.Sprintf("%d-W%d", year, month)
}
