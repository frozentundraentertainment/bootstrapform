package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputSelect -	<input> elements of type select...
const InputSelect InputType = "select"

// Select
//
//
// Checked		Boolean; if present, the checkbox is toggled on by default
type Select struct {
	inputBase
	Multiple       bool
	Size           int
	options        []*SelectOption
	SelectedOption *SelectOption
}

// Per Bootstrap
// <div class="form-check">  // Add form-check-inline for a horizontal layout
//  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
//  <label class="form-check-label" for="defaultCheck1">
//    Default checkbox
//  </label>
// </div>

// Render will add the appropriate input HTML tags to the HTML block
func (i *Select) Render(block *html.Block) {
	// Add default CSS if not ignored
	if !i.OverwriteCSS {
		// 	i.ClassAdd("form-control")
	}

	inputBlock, inputTag := i.inputBase.Render(block)
	i.Type = InputSelect

	if i.Multiple {
		inputTag.AddAttribute("multiple")
	}
	if i.Size > 0 {
		inputTag.AddAttributeWithData("size", i.Size)
	}

	if i.SelectedOption != nil {
		i.SelectedOption.AttributeAdd("selected", "")
	}

	for _, option := range i.options {
		option.Render(inputBlock)
	}
}

// AddOption will append the given options to the list
func (i *Select) AddOption(text string, selected bool) *SelectOption {
	option := new(SelectOption)
	option.Text = text
	option.Wrap = newWrapper()
	option.SpellCheck = true

	i.options = append(i.options, option)
	if selected {
		i.SelectedOption = option
	}
	return option
}
