package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

type validation struct {
	serverSide     bool
	IsValid        bool
	RenderValid    bool
	validMessage   *Block
	RenderInvalid  bool
	invalidMessage *Block
}

// ServerSide makes the input validation server side
func (v *validation) ServerSide() *validation {
	v.serverSide = true
	return v
}

// ClientSide makes the input validation client side
func (v *validation) ClientSide() *validation {
	v.serverSide = false
	return v
}

// AddValidFeedback will add a HTML block that will be rendered after the input
func (v *validation) AddValidFeedback(tag string, text string) *Block {
	v.validMessage = newBlock(tag, text)
	v.validMessage.ClassAdd("valid-feedback")
	v.RenderValid = true
	return v.validMessage
}

// AddInvalidFeedback will add a HTML block that will be rendered after the input
func (v *validation) AddInvalidFeedback(tag string, text string) *Block {
	v.invalidMessage = newBlock(tag, text)
	v.invalidMessage.ClassAdd("invalid-feedback")
	v.RenderInvalid = true
	return v.invalidMessage
}

// AddValidFeedback will add a HTML block that will be rendered after the input
func (v *validation) AddValidTooltip(tag string, text string) *Block {
	v.validMessage = newBlock(tag, text)
	v.validMessage.ClassAdd("valid-tooltip")
	v.RenderValid = true
	return v.validMessage
}

// AddInvalidFeedback will add a HTML block that will be rendered after the input
func (v *validation) AddInvalidTooltip(tag string, text string) *Block {
	v.invalidMessage = newBlock(tag, text)
	v.invalidMessage.ClassAdd("invalid-tooltip")
	v.RenderInvalid = true
	return v.invalidMessage
}

func (v *validation) Render(inputGroup *html.Block) {
	if v.RenderValid {
		v.validMessage.Render(inputGroup)
	}
	if v.RenderInvalid {
		v.invalidMessage.Render(inputGroup)
	}
}
