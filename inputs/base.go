package inputs

import (
	"crypto/rand"
	"strconv"
	"strings"

	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

type order int

const (
	OrderDefault order = iota
	OrderLabelInputHint
	OrderLabelHintInput
	OrderInputLabelHint
	OrderInputHintLabel
	OrderHintLabelInput
	OrderHintInputLabel
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input

type InputType string

func (i InputType) String() string {
	return string(i)
}

type Input interface {
	Render(*html.Block)
	GenerateID(int)
}

// Input Base
//
//
// Attribute	Description
// autocomplete	A string indicating the type of autocomplete functionality, if
// any, to allow on the input
//
// autofocus	A Boolean which, if present, makes the input take focus when the
// form is presented
//
// disabled		A Boolean attribute which is present if the input should be disabled
//
// form			The id of the <form> of which the input is a member; if absent,
// the input is a member of the nearest containing form, or is not a member of a
// form at all
//
// list			The id of a <datalist> element that provides a list of suggested
// values for the input
//
// name			The input's name, to identify the input in the data submitted
// with the form's data
//
// readonly		A Boolean attribute which, if true, indicates that the input
// cannot be edited
//
// required		A Boolean which, if true, indicates that the input must have a
// value before the form can be submitted
//
// tabindex		A numeric value providing guidance to the user agent as to the
// order in which controls receive focus when the user presses the Tab key
//
// type			A string indicating which input type the <input> element represents
//
// value		The input's current value
//
// pattern		A regular expression the input's contents must match in order to be valid
//
// placeholder	An exemplar value to display in the input field whenever it is empty
type inputBase struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	Label                    label
	Hint                     hint
	OverwriteCSS             bool
	BootstrapCustom          bool
	AutoComplete             bool
	AutoFocus                bool
	Disabled                 bool
	Form                     *string
	DataList                 uint
	ReadOnly                 bool
	Required                 bool
	Type                     InputType
	Value                    string
	Pattern                  string
	Placeholder              string
	Wrap                     wrapper
	WrapAll                  wrapper
	Order                    order
	FormGroup                FormGroup
	EnableFGWithoutLabelHint bool
	GroupNumber              int
	InputNumber              int
	preBlock                 *Block
	InputGroup               inputGroup
	postBlock                *Block
	Validation               validation
}

// init will configure a few default settings
func (i *inputBase) init(form *string, inputNum int, groupNum int, name string, label string, hint string) {
	i.Form = form
	i.InputNumber = inputNum
	i.GroupNumber = groupNum
	i.Name = name
	i.SpellCheck = true
	i.Label = newLabel(label)
	i.Hint = newHint(hint)
	i.Wrap = newWrapper()
	i.WrapAll = newWrapper()
	i.FormGroup = NewFormGroup()
	i.InputGroup = newInputGroup()
}

// AddPreBlock will add a HTML block that will be rendered before the input
func (i *inputBase) AddPreBlock(tag string, text string) *Block {
	i.preBlock = newBlock(tag, text)
	return i.preBlock
}

// AddPostBlock will add a HTML block that will be rendered after the input
func (i *inputBase) AddPostBlock(tag string, text string) *Block {
	i.postBlock = newBlock(tag, text)
	return i.postBlock
}

// GenerateID will assign a presumably unique ID
//noinspection GoRedundantConversion
func (i *inputBase) GenerateID(n int) {

	// Do not generate an ID if it is already set
	if i.ID != "" {
		return
	}

	// Start creating a new ID
	newID := new(strings.Builder)
	newID.WriteString(*i.Form + "_" + strconv.Itoa(i.GroupNumber) + "." + strconv.Itoa(i.InputNumber) + "_" + i.Type.String())

	if i.Name != "" {
		// i.ID = fmt.Sprintf("_form_%d_%s_%s", i.Form, i.Type, strings.Replace(i.Name, " ", "", -1))
		newID.WriteString("-" + strings.Replace(i.Name, " ", "", -1))
		i.ID = newID.String()
		return
	}

	output := make([]byte, n)

	// We will take n bytes, one byte for each character of output.
	randomness := make([]byte, n)

	// read all random
	_, err := rand.Read(randomness)
	if err != nil {
		panic(err)
	}
	l := len(letterBytes)

	// fill output
	for pos := range output {
		// get random item
		random := uint8(randomness[pos])
		// random % 64
		randomPos := random % uint8(l)
		// put into output
		output[pos] = letterBytes[randomPos]
	}

	// Build ID
	// i.ID = fmt.Sprintf("_form_%d_%s_%s-%s", i.Form, i.Name, i.Type.String(), output)
	newID.WriteString("-" + string(output))
}

// Render builds the common input attributes
func (i *inputBase) Render(inputGroup *html.Block) (*html.Block, *html.Tag) {

	// Generate an pseudo unique ID if not provided
	i.GenerateID(5)

	// If the associated label does not have an ID, set one
	// if i.Label.ID == "" && i.Label.Text != "" {
	// 	i.Label.ID = i.ID + ".label"
	// }

	// If the associated hint does not have an ID, set one
	// if i.Hint.ID == "" && i.Hint.Text != "" {
	// 	i.Hint.ID = i.ID + ".hint"
	// }

	// Check if default order
	if i.Order == OrderDefault {
		i.Order = OrderLabelInputHint
	}

	// Check if default INPUT CSS
	if !i.OverwriteCSS {
		switch i.Type {
		case InputCheckBox:
			if i.BootstrapCustom {
				i.ClassAdd("custom-control-input")
			} else {
				i.ClassAdd("form-check-input")
			}
		case InputRadio:
			if i.BootstrapCustom {
				// i.ClassAdd("custom-control")
				i.ClassAdd("custom-control-input")
			} else {
				i.ClassAdd("form-check-input")
			}
		case InputFile:
			if i.BootstrapCustom {
				i.ClassAdd("custom-file-input")
			} else {
				i.ClassAdd("form-control-file")
			}
		case InputRange:
			if i.BootstrapCustom {
				i.ClassAdd("custom-range")
			} else {
				i.ClassAdd("form-control-range")
			}
		case InputSelect:
			if i.BootstrapCustom {
				i.ClassAdd("custom-select")
			} else {
				i.ClassAdd("form-control")
			}
		case InputFieldSet:
		default:
			if i.BootstrapCustom {
				i.ClassAdd("custom-control-input")
			} else {
				i.ClassAdd("form-control")
			}
		}
	}

	// Check if default LABEL CSS
	if !i.Label.OverwriteCSS {
		switch i.Type {
		case InputCheckBox:
			if i.BootstrapCustom {
				i.Label.ClassAdd("custom-control-label")
			} else {
				i.Label.ClassAdd("form-check-label")
			}
		case InputRadio:
			if i.BootstrapCustom {
				i.Label.ClassAdd("custom-control-label")
			} else {
				i.Label.ClassAdd("form-check-label")
			}
		case InputFile:
			if i.BootstrapCustom {
				i.Label.ClassAdd("custom-file-label")
			} else {
			}
		case InputRange:
			if i.BootstrapCustom {
				i.Label.ClassAdd("custom-control-label")
			} else {
			}
		case InputSelect:
			if i.BootstrapCustom {
				i.Label.ClassAdd("custom-control-label")
			} else {
			}
		default:
			if i.BootstrapCustom {
				i.Label.ClassAdd("custom-control-label")
			} else {
			}
		}
	}

	// Check if default HINT CSS
	// if !i.Hint.OverwriteCSS {
	// 	switch i.Type {
	// 	case InputCheckBox:
	// 		if i.BootstrapCustom {
	// 			i.ClassAdd("custom-control-input")
	// 			i.Label.ClassAdd("custom-control-label")
	// 			i.FormGroup.ClassAdd("custom-control")
	// 			i.FormGroup.ClassAdd("custom-checkbox")
	// 		} else {
	// 			i.ClassAdd("form-control")
	// 			i.ClassAdd("form-check-input")
	// 			i.Label.ClassAdd("form-check-label")
	// 			i.FormGroup.ClassAdd("form-check")
	// 		}
	// 	case InputRadio:
	// 		if i.BootstrapCustom {
	// 			i.ClassAdd("custom-control-input")
	// 			i.Label.ClassAdd("custom-control-label")
	// 			i.FormGroup.ClassAdd("custom-control")
	// 			i.FormGroup.ClassAdd("custom-radio")
	// 		} else {
	// 			i.ClassAdd("form-control")
	// 			i.ClassAdd("form-check-input")
	// 			i.Label.ClassAdd("form-check-label")
	// 			i.FormGroup.ClassAdd("form-check")
	// 		}
	// 	case InputFile:
	// 		if i.BootstrapCustom {
	// 			i.ClassAdd("custom-file-input")
	// 			i.Label.ClassAdd("custom-file-label")
	// 			i.FormGroup.ClassAdd("custom-file")
	// 		} else {
	// 			i.ClassAdd("form-control")
	// 			i.FormGroup.ClassAdd("form-group")
	// 		}
	// 	case InputRange:
	// 		if i.BootstrapCustom {
	// 			i.ClassAdd("custom-range")
	// 		} else {
	// 			i.ClassAdd("form-control")
	// 			i.FormGroup.ClassAdd("form-group")
	// 		}
	// 	case InputSelect:
	// 		if i.BootstrapCustom {
	// 			i.ClassAdd("custom-select")
	// 		} else {
	// 			i.ClassAdd("form-control")
	// 			i.FormGroup.ClassAdd("form-group")
	// 		}
	// 	default:
	// 		if i.BootstrapCustom {
	// 			i.ClassAdd("custom-control-input")
	// 			i.Label.ClassAdd("custom-control-label")
	// 			i.FormGroup.ClassAdd("custom-control")
	// 		} else {
	// 			i.ClassAdd("form-control")
	// 			i.FormGroup.ClassAdd("form-group")
	// 		}
	// 	}
	// }

	// If the form group has been initialized, render it
	formGroup := inputGroup
	if i.FormGroup.Enable {

		// Check if default FORMGROUP CSS
		if !i.FormGroup.OverwriteCSS {
			switch i.Type {
			case InputCheckBox:
				if i.BootstrapCustom {
					i.FormGroup.ClassAdd("custom-control")
					i.FormGroup.ClassAdd("custom-checkbox")
				} else {
					i.FormGroup.ClassAdd("form-check")
				}
			case InputRadio:
				if i.BootstrapCustom {
					i.FormGroup.ClassAdd("custom-control")
					i.FormGroup.ClassAdd("custom-radio")
				} else {
					i.FormGroup.ClassAdd("form-check")
				}
			case InputFile:
				if i.BootstrapCustom {
					i.FormGroup.ClassAdd("custom-file")
				} else {
					i.FormGroup.ClassAdd("form-group")
				}
			case InputRange:
				if i.BootstrapCustom {
					i.FormGroup.ClassAdd("form-group")
				} else {
					i.FormGroup.ClassAdd("form-group")
				}
			case InputSelect:
				if i.BootstrapCustom {
					i.FormGroup.ClassAdd("form-group")
				} else {
					i.FormGroup.ClassAdd("form-group")
				}
			default:
				if i.BootstrapCustom {
					i.FormGroup.ClassAdd("custom-control")
				} else {
					i.FormGroup.ClassAdd("form-group")
				}
			}
		}

		if i.Label.Text != "" || i.Hint.Text != "" || i.EnableFGWithoutLabelHint {
			formGroup = i.FormGroup.Render(inputGroup)
			// var divTag *html.Tag
			// FormGroup, divTag = inputGroup.AddBlock("div")
			// divTag.AddAttributeWithData("class", strings.Join(i.formGroupClasses, " "))
			// if i.ID != "" {
			// 	divTag.AddAttributeWithData("id", "fg_"+i.ID)
			// }
		}
	}

	if i.Validation.serverSide {
		if i.Validation.IsValid {
			i.ClassAdd("is-valid")
		} else {
			i.ClassAdd("is-invalid")
		}
	}

	allWrap := i.WrapAll.Render(formGroup)

	var block *html.Block
	var tag *html.Tag
	switch i.Order {
	case OrderLabelInputHint:
		i.Label.Render(i.Label.Wrap.Render(allWrap), i.ID)
		block, tag = i.InputGroup.Render(allWrap, i)
		i.Hint.Render(i.Hint.Wrap.Render(allWrap))
	case OrderLabelHintInput:
		i.Label.Render(i.Label.Wrap.Render(allWrap), i.ID)
		i.Hint.Render(i.Hint.Wrap.Render(allWrap))
		block, tag = i.InputGroup.Render(allWrap, i)
	case OrderInputLabelHint:
		block, tag = i.InputGroup.Render(allWrap, i)
		i.Label.Render(i.Label.Wrap.Render(allWrap), i.ID)
		i.Hint.Render(i.Hint.Wrap.Render(allWrap))
	case OrderInputHintLabel:
		block, tag = i.InputGroup.Render(allWrap, i)
		i.Hint.Render(i.Hint.Wrap.Render(allWrap))
		i.Label.Render(i.Label.Wrap.Render(allWrap), i.ID)
	case OrderHintLabelInput:
		i.Hint.Render(i.Hint.Wrap.Render(allWrap))
		i.Label.Render(i.Label.Wrap.Render(allWrap), i.ID)
		block, tag = i.InputGroup.Render(allWrap, i)
	case OrderHintInputLabel:
		i.Hint.Render(i.Hint.Wrap.Render(allWrap))
		block, tag = i.InputGroup.Render(allWrap, i)
		i.Label.Render(i.Label.Wrap.Render(allWrap), i.ID)
	}
	if i.Type == InputCheckBox || i.Type == InputRadio || i.Type == InputTextArea || i.Type == InputSelect {
		i.Validation.Render(allWrap)
	} else {
		i.Validation.Render(block)
	}

	return block, tag
}

// Render Input
func (i *inputBase) renderInput(inputGrouping *html.Block) (*html.Block, *html.Tag) {

	// Create the input tag on the form-group, wrapping if necessary
	var blockTag string
	switch i.Type {
	case InputSelect:
		blockTag = i.Type.String()
		i.Type = ""
		defer func(i *inputBase) { i.Type = InputSelect }(i)
	case InputTextArea:
		blockTag = i.Type.String()
		i.Type = ""
		defer func(i *inputBase) { i.Type = InputTextArea }(i)
	case InputFieldSet:
		blockTag = i.Type.String()
		i.Type = ""
		defer func(i *inputBase) { i.Type = InputFieldSet }(i)
	default:
		blockTag = "input"
	}
	wrapBlock := i.Wrap.Render(inputGrouping)

	// Render the Pre Input HTML Block
	if i.preBlock != nil {
		i.preBlock.Render(wrapBlock)
	}

	block, tag := wrapBlock.AddBlock(blockTag)

	// Render base attributes
	tag.AddAttributeWithData("type", i.Type)
	if i.AutoComplete {
		tag.AddAttribute("autocomplete")
	}
	if i.AutoFocus {
		tag.AddAttribute("AutoFocus")
	}
	if i.Disabled {
		tag.AddAttribute("disabled")
	}
	// Disabled because all of our inputs will be in the form
	// if *i.Form != "" {
	// 	tag.AddAttributeWithData("form", i.Form)
	// }
	if i.DataList > 0 {
		tag.AddAttributeWithData("datalist", i.DataList)
	}
	tag.AddAttributeWithData("name", i.Name)
	if i.ReadOnly {
		tag.AddAttribute("readonly")
	}
	if i.Required {
		tag.AddAttribute("required")
	}
	tag.AddAttributeWithData("value", i.Value)
	tag.AddAttributeWithData("pattern", i.Pattern)
	tag.AddAttributeWithData("placeholder", i.Placeholder)

	tag.AddAttributeWithData("aria-describedby", i.Hint.ID)

	// Render Global attributes
	i.Attributes.Render(tag)
	i.Events.Render(tag)

	// Render the Post Input HTML Block
	if i.postBlock != nil {
		i.postBlock.Render(inputGrouping)
	}

	return block, tag
}
