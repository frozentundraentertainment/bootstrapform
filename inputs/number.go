package inputs

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// InputNumber -	HTML5 Only <input> elements of type number are used to let
// 					the user enter a number. They include built-in validation to
// 					reject non-numerical entries. The browser may opt to provide
// 					stepper arrows to let the user increase and decrease the
// 					value using their mouse or by simply tapping with a fingertip.
const InputNumber InputType = "number"

// Number
//
//
// HTML5 Only
//
// max	The maximum value to accept for this input
//
// min	The minimum value to accept for this input
//
// step	A stepping interval to use when using up and down arrows to adjust the
// 		value, as well as for validation
type Number struct {
	inputBase
	max  int
	min  int
	Step int
}

// Render will add the appropriate input HTML tags to the HTML block
func (i *Number) Render(block *html.Block) {
	i.inputBase.Render(block)
}
