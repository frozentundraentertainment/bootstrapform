package types

type Encoding string

// EncodeURL submits the form using URL encoded values
const EncodeURL Encoding = "application/x-www-form-urlencoded"

// EncodeData submits the form as Multipart Form Data, useful for sending files
const EncodeData Encoding = "multipart/form-data"

// EncodePlain submits the form as plain text
const EncodePlain Encoding = "text/plain"
