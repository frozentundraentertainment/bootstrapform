package types

import (
	"net/http"
)

type HttpMethod string

func (h HttpMethod) String() string {
	return string(h)
}

// MethodGet is a http.MethodGet
const MethodGet HttpMethod = http.MethodGet

// MethodPost is a http.MethodPost
const MethodPost HttpMethod = http.MethodPost

// MethodPatch is a http.MethodPatch
const MethodPatch HttpMethod = http.MethodPatch

// MethodDelete is a http.MethodDelete
const MethodDelete HttpMethod = http.MethodDelete

// MethodConnect is a http.MethodConnect
const MethodConnect HttpMethod = http.MethodConnect

// MethodHead is a http.MethodHead
const MethodHead HttpMethod = http.MethodHead

// MethodOptions is a http.MethodOptions
const MethodOptions HttpMethod = http.MethodOptions

// MethodTrace is a http.MethodTrace
const MethodTrace HttpMethod = http.MethodTrace

// MethodPut is a http.MethodPut
const MethodPut HttpMethod = http.MethodPut
