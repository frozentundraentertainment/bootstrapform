package types

type Target string

// TargetBlank - The response is displayed in a new window or tab
const TargetBlank Target = "_blank"

// TargetSelf - The response is displayed in the same frame (this is default)
const TargetSelf Target = "_self"

// TargetParent - The response is displayed in the parent frame
const TargetParent Target = "_parent"

// TargetTop - The response is displayed in the full body of the window
const TargetTop Target = "_top"

// TargetIFrame - The response is displayed in a named iframe
//
// This option is automatically set if you specify an IFrameName
const TargetIFrame Target = "_iframe"
