package bootstrapform

import (
	"fmt"
	"strings"

	"gitlab.com/frozentundraentertainment/bootstrapform/HTMLGlobals"
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
	"gitlab.com/frozentundraentertainment/bootstrapform/inputs"
	"gitlab.com/frozentundraentertainment/bootstrapform/types"
)

// NewForm prepares a new form
func NewForm(name string, action string) (*form, *inputs.InputArray) {
	form := new(form)
	form.Name = name
	form.Method = types.MethodPost
	form.Target = types.TargetSelf
	form.EncType = types.EncodeURL
	form.Action = action
	form.AutoComplete = true
	form.SpellCheck = true

	return form, form.AddInputGroup()
}

// form
// Attribute		Value								HTML5
// accept			file_type							Not supported in HTML5.
// Specifies a comma-separated list of file types  that the server accepts
// (that can be submitted through the file upload)
//
// accept-charset	character_set
// Specifies the character encodings that are to be used for the form submission
//
// action			URL
// Specifies where to send the form-data when a form is submitted
//
// autocomplete		on/off								HTML5 Only
// Specifies whether a form should have autocomplete on or off
//
// enctype			application/x-www-form-urlencoded
// 					multipart/form-data
// 					text/plain
// Specifies how the form-data should be encoded when submitting it to the server
// (only for method="post")
//
// method			get
// 					post
// Specifies the HTTP method to use when sending form-data
//
// name				text
// Specifies the name of a form
//
// novalidate		novalidate							HTML5 Only
// Specifies that the form should not be validated when submitted
//
// target			_blank
// 					_self
// 					_parent
// 					_top
// 	Specifies where to display the response that is received after submitting
// 	the form
type form struct {
	HTMLGlobals.Attributes
	HTMLGlobals.Events
	HTMLGlobals.CustomHTML
	Name            string
	OverwriteCSS    bool
	Method          types.HttpMethod
	Action          string
	AcceptMimeTypes []string
	AcceptCharset   []string
	AutoComplete    bool
	EncType         types.Encoding
	NoValidate      bool
	Target          types.Target
	iFrameName      string
	Inputs          []*inputs.InputArray
	// PreGroupInputs  inputs.InputArray
	// groups          []*formGroup
	// PostGroupInputs inputs.InputArray
	submit        *inputs.Button
	IndentLevel   int
	IndentChar    string
	DisableIndent bool
}

// AddMimeType adds a MIME type to the accept form header
func (f *form) AddMimeType(mime string) *form {
	f.AcceptMimeTypes = append(f.AcceptMimeTypes, mime)
	return f
}

// AddCharset adds a Char set to the accept-charset form header
func (f *form) AddCharset(char string) *form {
	f.AcceptCharset = append(f.AcceptCharset, char)
	return f
}

// TargetIFrame will specify an iFrame as the target of the form
func (f *form) TargetIFrame(name string) *form {
	f.iFrameName = name
	f.Target = types.TargetIFrame
	return f
}

// AddFormGroup will create a new form group and associate it with this form
// func (f *form) AddFormGroup(id string) *formGroup {
// 	group := new(formGroup)
// 	group.parentForm = f
// 	if id == "" {
// 		id = fmt.Sprintf("%s-group%d", f.ID, len(f.groups))
// 	}
// 	group.ID = id
// 	group.SpellCheck = f.SpellCheck
//
// 	f.groups = append(f.groups, group)
//
// 	return group
// }

// AddInputGroup will add another input array to the slice of inputs
func (f *form) AddInputGroup() *inputs.InputArray {
	inputGroup := inputs.NewInputGroup(&f.ID, len(f.Inputs))
	f.Inputs = append(f.Inputs, inputGroup)
	return inputGroup
}

// AddSubmitButton will add a submit button to the form
func (f *form) AddSubmitButton(text string) *inputs.Button {
	f.submit = new(inputs.Button)
	f.submit.Type = inputs.BtnSubmit
	f.submit.Text = text
	f.submit.SpellCheck = true
	f.submit.AutoComplete = true
	f.submit.FormGroup = inputs.NewFormGroup()
	return f.submit
}

// SetIndentLevel will set the current indent formatting.
// The number given is the level for the opening tag
func (f *form) SetIndentLevel(level int) *form {
	f.IndentLevel = level
	return f
}

// SetIndentCharacter will be the string used for indentation.
// It does not need to be a single character and can be a full string.
func (f *form) SetIndentCharacter(char string) *form {
	f.IndentChar = char
	return f
}

// Render will return a string representation of the form
func (f *form) Render() (string, error) {
	// Setup
	if f.Name == "" {
		f.Name = "form1"
	}
	if f.ID == "" {
		f.ID = fmt.Sprintf("form_%s", strings.Replace(strings.ToLower(f.Name), " ", "_", -1))
	}
	if f.Target == types.TargetIFrame {
		f.Target = types.Target(f.iFrameName)
	}
	if f.DisableIndent {
		f.IndentChar = ""
		f.IndentLevel = 0
	} else if f.IndentChar == "" {
		f.IndentChar = "    "
	}

	// Build Form
	form, formTag := html.New(new(strings.Builder), "form", f.IndentLevel, &f.IndentChar)
	if f.BeforeOpenTag != "" {
		form.RawHTML.BeforeOpenTag = f.BeforeOpenTag + "\n"
	}
	if f.AfterOpenTag != "" {
		form.RawHTML.AfterOpenTag = "\n" + f.AfterOpenTag + "\n"
	}
	if f.BeforeEndTag != "" {
		form.RawHTML.BeforeEndTag = "\n" + f.BeforeEndTag + "\n"
	}
	if f.AfterEndTag != "" {
		form.RawHTML.AfterEndTag = "\n" + f.AfterEndTag
	}

	// Render base form
	formTag.AddAttributeWithData("name", f.Name)
	formTag.AddAttributeWithData("action", f.Action)
	formTag.AddAttributeWithData("target", f.Target)

	if f.Method != types.MethodGet {
		methodIG := f.AddInputGroup()
		formMethod := methodIG.AddText("_method", "", "")
		formMethod.Hidden = true
		formMethod.ReadOnly = true
		formMethod.Value = f.Method.String()

		formTag.AddAttributeWithData("method", strings.ToLower(types.MethodPost.String()))
		formTag.AddAttributeWithData("enctype", f.EncType)
	} else {
		formTag.AddAttributeWithData("method", strings.ToLower(types.MethodGet.String()))
	}

	if !f.AutoComplete {
		formTag.AddAttributeWithData("autocomplete", "off")
	}

	if f.NoValidate {
		formTag.AddAttribute("novalidate")
	}

	if !f.OverwriteCSS {
	}

	formTag.AddAttributeWithData("accept", strings.Join(f.AcceptMimeTypes, ","))
	formTag.AddAttributeWithData("accept-charset", strings.Join(f.AcceptCharset, ","))

	// Todo: Add CSRF

	// Render Global attributes
	f.Attributes.Render(formTag)
	f.Events.Render(formTag)

	// PreGroup Inputs
	// f.PreGroupInputs.Render(form)

	// Render form contents
	// for _, group := range f.groups {
	// 	group.Render(form)
	// }

	// PostGroup Inputs
	// f.PostGroupInputs.Render(form)

	// Render inputs
	for _, inputGroup := range f.Inputs {
		inputGroup.Render(form)
	}

	// Render submit button if configured
	if f.submit != nil {
		f.submit.Render(form)
	}

	// Render HTML Block
	errTag, err := form.Render()
	if err != nil {
		return errTag, err
	}
	return form.Parent.String(), nil
}
