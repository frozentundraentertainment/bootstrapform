package radio

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#inline
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleRadioInline() {

	// Identity Form
	testForm, inputGroup := bootstrapform.NewForm("Radio Inline", "")

	radio1 := inputGroup.AddRadio("inlineRadioOptions", "1", "")
	radio1.Inline = true
	radio1.ID = "inlineradio1"
	radio1.Value = "option1"

	radio2 := inputGroup.AddRadio("inlineRadioOptions", "2", "")
	radio2.Inline = true
	radio2.ID = "inlineradio2"
	radio2.Value = "option2"

	radio3 := inputGroup.AddRadio("inlineRadioOptions", "3 (disabled)", "")
	radio3.Inline = true
	radio3.ID = "inlineradio3"
	radio3.Value = "option3"
	radio3.Disabled = true

	// Render the form
	depends.Render("radioinline", testForm)
	// Output:
	// <form name="Radio Inline" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_radio_inline">
	//     <div class="form-check-inline form-check">
	//         <input type="radio" name="inlineRadioOptions" value="option1" class="form-check-input" id="inlineradio1">
	//         <label for="inlineradio1" class="form-check-label">1</label>
	//     </div>
	//     <div class="form-check-inline form-check">
	//         <input type="radio" name="inlineRadioOptions" value="option2" class="form-check-input" id="inlineradio2">
	//         <label for="inlineradio2" class="form-check-label">2</label>
	//     </div>
	//     <div class="form-check-inline form-check">
	//         <input type="radio" disabled name="inlineRadioOptions" value="option3" class="form-check-input" id="inlineradio3">
	//         <label for="inlineradio3" class="form-check-label">3 (disabled)</label>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_radio_inline_1.0_text-_method">
	// </form>
}
