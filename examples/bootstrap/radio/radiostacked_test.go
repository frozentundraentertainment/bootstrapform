package radio

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#default-stacked
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleRadioStacked() {

	// Identity Form
	testForm, inputGroup := bootstrapform.NewForm("Radio Stacked", "")

	radio1 := inputGroup.AddRadio("exampleRadios", "Default Radio", "")
	radio1.ID = "exampleRadios1"
	radio1.Value = "Option1"

	radio2 := inputGroup.AddRadio("exampleRadios", "Second Default Radio", "")
	radio2.ID = "exampleRadios2"
	radio2.Value = "Option2"

	radio3 := inputGroup.AddRadio("exampleRadios", "Disabled Radio", "")
	radio3.ID = "exampleRadios3"
	radio3.Value = "Option3"
	radio3.Disabled = true

	// Render the form
	depends.Render("radiostacked", testForm)
	// Output:
	// <form name="Radio Stacked" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_radio_stacked">
	//     <div class="form-check">
	//         <input type="radio" name="exampleRadios" value="Option1" class="form-check-input" id="exampleRadios1">
	//         <label for="exampleRadios1" class="form-check-label">Default Radio</label>
	//     </div>
	//     <div class="form-check">
	//         <input type="radio" name="exampleRadios" value="Option2" class="form-check-input" id="exampleRadios2">
	//         <label for="exampleRadios2" class="form-check-label">Second Default Radio</label>
	//     </div>
	//     <div class="form-check">
	//         <input type="radio" disabled name="exampleRadios" value="Option3" class="form-check-input" id="exampleRadios3">
	//         <label for="exampleRadios3" class="form-check-label">Disabled Radio</label>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_radio_stacked_1.0_text-_method">
	// </form>
}
