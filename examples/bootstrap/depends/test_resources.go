package depends

import (
	"fmt"
	"io/ioutil"
)

var preHTML = `<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<title>Sample HTML</title>
</head>
<body>
<div class="container">

<!-- Start rendered HTML -->
`

var postHTML = `
<!-- End rendered HTML -->

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</div>
</body>
</html>`

type Renderer interface {
	Render() (string, error)
}

// Render the given form to a file and the console
func Render(outFile string, form Renderer) {
	// Render the form
	formString, err := form.Render()
	if err != nil {
		fmt.Printf("Error: %v\n%s", err, formString)
	}

	err = ioutil.WriteFile(outFile+".html", []byte(fmt.Sprintf("%s\n%s\n%s", preHTML, formString, postHTML)), 0644)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\n%s", formString)
}
