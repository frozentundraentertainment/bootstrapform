package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#help-text
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleHelpTextInline() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Help Text Inline", "")
	testForm.ClassAdd("form-inline")

	// Add a password input
	password := inputGroup.AddPassword("", "Password", "Must be 8-20 characters long.")
	password.ID = "inputPassword6"
	password.Hint.ID = "passwordHelpInline"
	password.Hint.OverwriteCSS = true
	password.Hint.ClassAdd("text-muted")
	password.ClassAdd("mx-sm-3")

	// Render the form
	depends.Render("helptextinline", testForm)
	// Output:
	// <form name="Help Text Inline" target="_self" method="post" enctype="application/x-www-form-urlencoded" class="form-inline" id="form_help_text_inline">
	//     <div class="form-group">
	//         <label for="inputPassword6">Password</label>
	//         <input type="password" aria-describedby="passwordHelpInline" class="mx-sm-3 form-control" id="inputPassword6">
	//         <small class="text-muted" id="passwordHelpInline">Must be 8-20 characters long.</small>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_help_text_inline_1.0_text-_method">
	// </form>
}
