package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#help-text
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleHelpText() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Help Text", "")

	// Add a password input
	password := inputGroup.AddPassword("", "Password", "Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.")
	password.ID = "inputPassword5"
	password.Hint.ID = "passwordHelpBlock"

	// Render the form
	depends.Render("helptext", testForm)
	// Output:
	// <form name="Help Text" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_help_text">
	//     <div class="form-group">
	//         <label for="inputPassword5">Password</label>
	//         <input type="password" aria-describedby="passwordHelpBlock" class="form-control" id="inputPassword5">
	//         <small class="form-text text-muted" id="passwordHelpBlock">Your password must be 8-20 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.</small>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_help_text_1.0_text-_method">
	// </form>
}
