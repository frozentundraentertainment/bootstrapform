package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
	"gitlab.com/frozentundraentertainment/bootstrapform/inputs"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#supported-elements
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleValidationSupportedElements() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Validation Supported Elements", "")
	testForm.ClassAdd("was-validated")

	// Add a Textarea
	textarea := inputGroup.AddTextArea("", "Textarea", "")
	textarea.ID = "validationTextarea"
	textarea.Placeholder = "Required example textarea"
	textarea.FormGroup.OverwriteCSS = true
	textarea.FormGroup.ClassAdd("mb-3")
	textarea.Required = true
	textarea.Validation.ServerSide().AddInvalidFeedback("div", "Please enter a message in the textarea.")

	// Add a custom checkbox
	checkbox := inputGroup.AddCheckBox("", "Check this custom checkbox", "")
	checkbox.ID = "customControlValidation1"
	checkbox.Required = true
	checkbox.BootstrapCustom = true
	checkbox.FormGroup.ClassAdd("mb-3")
	checkbox.Validation.AddInvalidFeedback("div", "Example invalid feedback text")

	// Add custom radio #1
	radio1 := inputGroup.AddRadio("radio-stacked", "Toggle this custom radio", "")
	radio1.ID = "customControlValidation2"
	radio1.Required = true
	radio1.BootstrapCustom = true

	// Add custom radio #2
	radio2 := inputGroup.AddRadio("radio-stacked", "Or toggle this other custom radio", "")
	radio2.ID = "customControlValidation3"
	radio2.Required = true
	radio2.BootstrapCustom = true
	radio2.FormGroup.ClassAdd("mb-3")
	radio2.Validation.AddInvalidFeedback("div", "More example invalid feedback text")

	// Add custom select
	mySelect := inputGroup.AddSelect("", "", "")
	mySelect.Required = true
	mySelect.BootstrapCustom = true
	mySelect.EnableFGWithoutLabelHint = true
	mySelect.AddOption("Open this select menu", false)
	mySelect.AddOption("One", false).Value = "1"
	mySelect.AddOption("Two", false).Value = "2"
	mySelect.AddOption("Three", false).Value = "3"
	mySelect.Validation.AddInvalidFeedback("div", "Example invalid custom select feedback")

	// Add custom file
	file := inputGroup.AddFile("", "Choose file...", "")
	file.ID = "validatedCustomFile"
	file.Required = true
	file.Order = inputs.OrderInputLabelHint
	file.BootstrapCustom = true
	file.Validation.AddInvalidFeedback("div", "Example invalid custom file feedback")

	// Add submit Button
	testForm.AddSubmitButton("Submit form")

	// Render the form
	depends.Render("validationcsupportedelements", testForm)
	// Output:
	// <form name="Validation Supported Elements" target="_self" method="post" enctype="application/x-www-form-urlencoded" class="was-validated" id="form_validation_supported_elements">
	//     <div class="mb-3">
	//         <label for="validationTextarea">Textarea</label>
	//         <textarea required placeholder="Required example textarea" class="form-control is-invalid" id="validationTextarea"></textarea>
	//         <div class="invalid-feedback">Please enter a message in the textarea.</div>
	//     </div>
	//     <div class="mb-3 custom-control custom-checkbox">
	//         <input type="checkbox" required class="custom-control-input" id="customControlValidation1">
	//         <label for="customControlValidation1" class="custom-control-label">Check this custom checkbox</label>
	//         <div class="invalid-feedback">Example invalid feedback text</div>
	//     </div>
	//     <div class="custom-control custom-radio">
	//         <input type="radio" name="radio-stacked" required class="custom-control-input" id="customControlValidation2">
	//         <label for="customControlValidation2" class="custom-control-label">Toggle this custom radio</label>
	//     </div>
	//     <div class="mb-3 custom-control custom-radio">
	//         <input type="radio" name="radio-stacked" required class="custom-control-input" id="customControlValidation3">
	//         <label for="customControlValidation3" class="custom-control-label">Or toggle this other custom radio</label>
	//         <div class="invalid-feedback">More example invalid feedback text</div>
	//     </div>
	//     <div class="form-group">
	//         <select required class="custom-select">
	//             <option>Open this select menu</option>
	//             <option value="1">One</option>
	//             <option value="2">Two</option>
	//             <option value="3">Three</option>
	//         </select>
	//         <div class="invalid-feedback">Example invalid custom select feedback</div>
	//     </div>
	//     <div class="custom-file">
	//         <input type="file" required class="custom-file-input" id="validatedCustomFile">
	//             <div class="invalid-feedback">Example invalid custom file feedback</div>
	//         <label for="validatedCustomFile" class="custom-file-label">Choose file...</label>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_validation_supported_elements_1.0_text-_method">
	//     <button type="submit" class="btn btn-primary">Submit form</button>
	// </form>
}
