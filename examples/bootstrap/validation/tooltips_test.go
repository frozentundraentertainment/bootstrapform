package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#tooltips
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleValidationTooltips() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Validation Tooltips", "")
	testForm.ClassAdd("needs-validation")
	testForm.NoValidate = true

	// Add a form row
	row1 := inputGroup.AddBlock("div", "")
	row1.ClassAdd("form-row")

	// Add a first name input
	fName := row1.Inputs.AddText("", "First name", "")
	fName.ID = "validationTooltip01"
	fName.Placeholder = "First name"
	fName.Value = "Mark"
	fName.Required = true
	fName.FormGroup.OverwriteCSS = true
	fName.FormGroup.ClassAdd("col-md-4")
	fName.FormGroup.ClassAdd("mb-3")
	fName.Validation.AddValidTooltip("div", "Looks good!")

	// Add a first name input
	lName := row1.Inputs.AddText("", "Last name", "")
	lName.ID = "validationTooltip02"
	lName.Placeholder = "Last name"
	lName.Value = "Otto"
	lName.Required = true
	lName.FormGroup.OverwriteCSS = true
	lName.FormGroup.ClassAdd("col-md-4")
	lName.FormGroup.ClassAdd("mb-3")
	lName.Validation.AddValidTooltip("div", "Looks good!")

	// Add an Username input
	username := row1.Inputs.AddText("", "Username", "")
	username.ID = "validationToolTipUsername"
	username.Placeholder = "Username"
	username.Required = true
	username.FormGroup.OverwriteCSS = true
	username.FormGroup.ClassAdd("col-md-4")
	username.FormGroup.ClassAdd("mb-3")
	_, usernameText := username.InputGroup.Prepend("div", "span", "@")
	usernameText.ID = "inputGroupTooltipUsernamePrepend"
	username.Validation.AddInvalidFeedback("div", "Please choose a unique and valid username.")

	// Add a form row
	row2 := inputGroup.AddBlock("div", "")
	row2.ClassAdd("form-row")

	// Add a city input
	city := row2.Inputs.AddText("", "City", "")
	city.ID = "validationTooltip03"
	city.Placeholder = "City"
	city.Required = true
	city.FormGroup.OverwriteCSS = true
	city.FormGroup.ClassAdd("col-md-6")
	city.FormGroup.ClassAdd("mb-3")
	city.Validation.AddInvalidFeedback("div", "Please provide a valid city.")

	// add a state input
	state := row2.Inputs.AddText("", "State", "")
	state.ID = "validationTooltip04"
	state.Placeholder = "State"
	state.Required = true
	state.FormGroup.OverwriteCSS = true
	state.FormGroup.ClassAdd("col-md-3")
	state.FormGroup.ClassAdd("mb-3")
	state.Validation.AddInvalidFeedback("div", "Please provide a valid state.")

	// add a zipcode input
	zip := row2.Inputs.AddText("", "Zip", "")
	zip.ID = "validationTooltip05"
	zip.Placeholder = "Zip"
	zip.Required = true
	zip.FormGroup.OverwriteCSS = true
	zip.FormGroup.ClassAdd("col-md-3")
	zip.FormGroup.ClassAdd("mb-3")
	zip.Validation.AddInvalidFeedback("div", "Please provide a valid zip.")

	// Add submit Button
	testForm.AddSubmitButton("Submit form")

	// Render the form
	depends.Render("validationtooltips", testForm)
	// Output:
	// <form name="Validation Tooltips" target="_self" method="post" enctype="application/x-www-form-urlencoded" novalidate class="needs-validation" id="form_validation_tooltips">
	//     <div class="form-row">
	//         <div class="col-md-4 mb-3">
	//             <label for="validationTooltip01">First name</label>
	//             <input type="text" required value="Mark" placeholder="First name" class="form-control" id="validationTooltip01">
	//                 <div class="valid-tooltip">Looks good!</div>
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationTooltip02">Last name</label>
	//             <input type="text" required value="Otto" placeholder="Last name" class="form-control" id="validationTooltip02">
	//                 <div class="valid-tooltip">Looks good!</div>
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationToolTipUsername">Username</label>
	//             <div class="input-group">
	//                 <div class="input-group-prepend">
	//                     <span class="input-group-text" id="inputGroupTooltipUsernamePrepend">@</span>
	//                 </div>
	//                 <input type="text" required placeholder="Username" class="form-control" id="validationToolTipUsername">
	//                     <div class="invalid-feedback">Please choose a unique and valid username.</div>
	//             </div>
	//         </div>
	//     </div>
	//     <div class="form-row">
	//         <div class="col-md-6 mb-3">
	//             <label for="validationTooltip03">City</label>
	//             <input type="text" required placeholder="City" class="form-control" id="validationTooltip03">
	//                 <div class="invalid-feedback">Please provide a valid city.</div>
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationTooltip04">State</label>
	//             <input type="text" required placeholder="State" class="form-control" id="validationTooltip04">
	//                 <div class="invalid-feedback">Please provide a valid state.</div>
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationTooltip05">Zip</label>
	//             <input type="text" required placeholder="Zip" class="form-control" id="validationTooltip05">
	//                 <div class="invalid-feedback">Please provide a valid zip.</div>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_validation_tooltips_1.0_text-_method">
	//     <button type="submit" class="btn btn-primary">Submit form</button>
	// </form>
}
