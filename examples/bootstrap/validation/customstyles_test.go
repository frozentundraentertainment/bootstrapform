package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#custom-styles
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleValidationCustomStyles() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Validation Custom Styles", "")
	testForm.ClassAdd("needs-validation")
	testForm.NoValidate = true

	// Add a form row
	row1 := inputGroup.AddBlock("div", "")
	row1.ClassAdd("form-row")

	// Add a first name input
	fName := row1.Inputs.AddText("", "First name", "")
	fName.ID = "validationCustom01"
	fName.Placeholder = "First name"
	fName.Value = "Mark"
	fName.Required = true
	fName.FormGroup.OverwriteCSS = true
	fName.FormGroup.ClassAdd("col-md-4")
	fName.FormGroup.ClassAdd("mb-3")
	fName.Validation.AddValidFeedback("div", "Looks good!")

	// Add a first name input
	lName := row1.Inputs.AddText("", "Last name", "")
	lName.ID = "validationCustom02"
	lName.Placeholder = "Last name"
	lName.Value = "Otto"
	lName.Required = true
	lName.FormGroup.OverwriteCSS = true
	lName.FormGroup.ClassAdd("col-md-4")
	lName.FormGroup.ClassAdd("mb-3")
	lName.Validation.AddValidFeedback("div", "Looks good!")

	// Add an Username input
	username := row1.Inputs.AddText("", "Username", "")
	username.ID = "validationCustomUsername"
	username.Placeholder = "Username"
	username.Required = true
	username.FormGroup.OverwriteCSS = true
	username.FormGroup.ClassAdd("col-md-4")
	username.FormGroup.ClassAdd("mb-3")
	_, usernameText := username.InputGroup.Prepend("div", "span", "@")
	usernameText.ID = "inputGroupPrepend"
	username.Validation.AddInvalidFeedback("div", "Please choose a username.")

	// Add a form row
	row2 := inputGroup.AddBlock("div", "")
	row2.ClassAdd("form-row")

	// Add a city input
	city := row2.Inputs.AddText("", "City", "")
	city.ID = "validationCustom03"
	city.Placeholder = "City"
	city.Required = true
	city.FormGroup.OverwriteCSS = true
	city.FormGroup.ClassAdd("col-md-6")
	city.FormGroup.ClassAdd("mb-3")
	city.Validation.AddInvalidFeedback("div", "Please provide a valid city.")

	// add a state input
	state := row2.Inputs.AddText("", "State", "")
	state.ID = "validationCustom04"
	state.Placeholder = "State"
	state.Required = true
	state.FormGroup.OverwriteCSS = true
	state.FormGroup.ClassAdd("col-md-3")
	state.FormGroup.ClassAdd("mb-3")
	state.Validation.AddInvalidFeedback("div", "Please provide a valid state.")

	// add a zipcode input
	zip := row2.Inputs.AddText("", "Zip", "")
	zip.ID = "validationCustom05"
	zip.Placeholder = "Zip"
	zip.Required = true
	zip.FormGroup.OverwriteCSS = true
	zip.FormGroup.ClassAdd("col-md-3")
	zip.FormGroup.ClassAdd("mb-3")
	zip.Validation.AddInvalidFeedback("div", "Please provide a valid zip.")

	// Add a checkbox input
	checkbox := inputGroup.AddCheckBox("", "Agree to terms and conditions", "")
	checkbox.ID = "invalidCheck"
	checkbox.Required = true
	checkbox.FormGroup.OverwriteCSS = true
	checkbox.FormGroup.ClassAdd("form-group")
	checkbox.WrapAll.AddClass("form-check")
	checkbox.Validation.AddInvalidFeedback("div", "You must agree before submitting.")

	// Add submit Button
	testForm.AddSubmitButton("Submit form")

	testForm.AfterEndTag = `
<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
			// Fetch all the forms we want to apply custom Bootstrap validation styles to
			var forms = document.getElementsByClassName('needs-validation');
			// Loop over them and prevent submission
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();
</script>`

	// Render the form
	depends.Render("validationcustomstyles", testForm)
	// Output:
	// <form name="Validation Custom Styles" target="_self" method="post" enctype="application/x-www-form-urlencoded" novalidate class="needs-validation" id="form_validation_custom_styles">
	//     <div class="form-row">
	//         <div class="col-md-4 mb-3">
	//             <label for="validationCustom01">First name</label>
	//             <input type="text" required value="Mark" placeholder="First name" class="form-control" id="validationCustom01">
	//                 <div class="valid-feedback">Looks good!</div>
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationCustom02">Last name</label>
	//             <input type="text" required value="Otto" placeholder="Last name" class="form-control" id="validationCustom02">
	//                 <div class="valid-feedback">Looks good!</div>
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationCustomUsername">Username</label>
	//             <div class="input-group">
	//                 <div class="input-group-prepend">
	//                     <span class="input-group-text" id="inputGroupPrepend">@</span>
	//                 </div>
	//                 <input type="text" required placeholder="Username" class="form-control" id="validationCustomUsername">
	//                     <div class="invalid-feedback">Please choose a username.</div>
	//             </div>
	//         </div>
	//     </div>
	//     <div class="form-row">
	//         <div class="col-md-6 mb-3">
	//             <label for="validationCustom03">City</label>
	//             <input type="text" required placeholder="City" class="form-control" id="validationCustom03">
	//                 <div class="invalid-feedback">Please provide a valid city.</div>
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationCustom04">State</label>
	//             <input type="text" required placeholder="State" class="form-control" id="validationCustom04">
	//                 <div class="invalid-feedback">Please provide a valid state.</div>
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationCustom05">Zip</label>
	//             <input type="text" required placeholder="Zip" class="form-control" id="validationCustom05">
	//                 <div class="invalid-feedback">Please provide a valid zip.</div>
	//         </div>
	//     </div>
	//     <div class="form-group">
	//         <div class="form-check">
	//             <input type="checkbox" required class="form-check-input" id="invalidCheck">
	//             <label for="invalidCheck" class="form-check-label">Agree to terms and conditions</label>
	//             <div class="invalid-feedback">You must agree before submitting.</div>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_validation_custom_styles_1.0_text-_method">
	//     <button type="submit" class="btn btn-primary">Submit form</button>
	// </form>
	//
	// <script>
	//	// Example starter JavaScript for disabling form submissions if there are invalid fields
	//	(function() {
	//		'use strict';
	//		window.addEventListener('load', function() {
	//			// Fetch all the forms we want to apply custom Bootstrap validation styles to
	//			var forms = document.getElementsByClassName('needs-validation');
	//			// Loop over them and prevent submission
	//			var validation = Array.prototype.filter.call(forms, function(form) {
	//				form.addEventListener('submit', function(event) {
	//					if (form.checkValidity() === false) {
	//						event.preventDefault();
	//						event.stopPropagation();
	//					}
	//					form.classList.add('was-validated');
	//				}, false);
	//			});
	//		}, false);
	//	})();
	// </script>
}
