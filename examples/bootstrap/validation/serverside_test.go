package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#server-side
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleValidationServerSide() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Validation Custom Styles", "")

	// Add a form row
	row1 := inputGroup.AddBlock("div", "")
	row1.ClassAdd("form-row")

	// Add a first name input
	fName := row1.Inputs.AddText("", "First name", "")
	fName.ID = "validationServer01"
	fName.Placeholder = "First name"
	fName.Value = "Mark"
	fName.Required = true
	fName.FormGroup.OverwriteCSS = true
	fName.FormGroup.ClassAdd("col-md-4")
	fName.FormGroup.ClassAdd("mb-3")
	fName.Validation.ServerSide().AddValidFeedback("div", "Looks good!")
	fName.Validation.IsValid = true

	// Add a first name input
	lName := row1.Inputs.AddText("", "Last name", "")
	lName.ID = "validationServer02"
	lName.Placeholder = "Last name"
	lName.Value = "Otto"
	lName.Required = true
	lName.FormGroup.OverwriteCSS = true
	lName.FormGroup.ClassAdd("col-md-4")
	lName.FormGroup.ClassAdd("mb-3")
	lName.Validation.ServerSide().AddValidFeedback("div", "Looks good!")
	lName.Validation.IsValid = true

	// Add an Username input
	username := row1.Inputs.AddText("", "Username", "")
	username.ID = "validationServerUsername"
	username.Placeholder = "Username"
	username.Required = true
	username.FormGroup.OverwriteCSS = true
	username.FormGroup.ClassAdd("col-md-4")
	username.FormGroup.ClassAdd("mb-3")
	_, usernameText := username.InputGroup.Prepend("div", "span", "@")
	usernameText.ID = "inputGroupPrepend3"
	username.Validation.ServerSide().AddInvalidFeedback("div", "Please choose a username.")

	// Add a form row
	row2 := inputGroup.AddBlock("div", "")
	row2.ClassAdd("form-row")

	// Add a city input
	city := row2.Inputs.AddText("", "City", "")
	city.ID = "validationServer03"
	city.Placeholder = "City"
	city.Required = true
	city.FormGroup.OverwriteCSS = true
	city.FormGroup.ClassAdd("col-md-6")
	city.FormGroup.ClassAdd("mb-3")
	city.Validation.ServerSide().AddInvalidFeedback("div", "Please provide a valid city.")

	// add a state input
	state := row2.Inputs.AddText("", "State", "")
	state.ID = "validationServer04"
	state.Placeholder = "State"
	state.Required = true
	state.FormGroup.OverwriteCSS = true
	state.FormGroup.ClassAdd("col-md-3")
	state.FormGroup.ClassAdd("mb-3")
	state.Validation.ServerSide().AddInvalidFeedback("div", "Please provide a valid state.")

	// add a zipcode input
	zip := row2.Inputs.AddText("", "Zip", "")
	zip.ID = "validationServer05"
	zip.Placeholder = "Zip"
	zip.Required = true
	zip.FormGroup.OverwriteCSS = true
	zip.FormGroup.ClassAdd("col-md-3")
	zip.FormGroup.ClassAdd("mb-3")
	zip.Validation.ServerSide().AddInvalidFeedback("div", "Please provide a valid zip.")

	// Add a checkbox input
	checkbox := inputGroup.AddCheckBox("", "Agree to terms and conditions", "")
	checkbox.ID = "invalidCheck3"
	checkbox.Required = true
	checkbox.FormGroup.OverwriteCSS = true
	checkbox.FormGroup.ClassAdd("form-group")
	checkbox.WrapAll.AddClass("form-check")
	checkbox.Validation.ServerSide().AddInvalidFeedback("div", "You must agree before submitting.")

	// Add submit Button
	testForm.AddSubmitButton("Submit form")

	// Render the form
	depends.Render("validationserverside", testForm)
	// Output:
	// <form name="Validation Custom Styles" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_validation_custom_styles">
	//     <div class="form-row">
	//         <div class="col-md-4 mb-3">
	//             <label for="validationServer01">First name</label>
	//             <input type="text" required value="Mark" placeholder="First name" class="form-control is-valid" id="validationServer01">
	//                 <div class="valid-feedback">Looks good!</div>
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationServer02">Last name</label>
	//             <input type="text" required value="Otto" placeholder="Last name" class="form-control is-valid" id="validationServer02">
	//                 <div class="valid-feedback">Looks good!</div>
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationServerUsername">Username</label>
	//             <div class="input-group">
	//                 <div class="input-group-prepend">
	//                     <span class="input-group-text" id="inputGroupPrepend3">@</span>
	//                 </div>
	//                 <input type="text" required placeholder="Username" class="form-control is-invalid" id="validationServerUsername">
	//                     <div class="invalid-feedback">Please choose a username.</div>
	//             </div>
	//         </div>
	//     </div>
	//     <div class="form-row">
	//         <div class="col-md-6 mb-3">
	//             <label for="validationServer03">City</label>
	//             <input type="text" required placeholder="City" class="form-control is-invalid" id="validationServer03">
	//                 <div class="invalid-feedback">Please provide a valid city.</div>
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationServer04">State</label>
	//             <input type="text" required placeholder="State" class="form-control is-invalid" id="validationServer04">
	//                 <div class="invalid-feedback">Please provide a valid state.</div>
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationServer05">Zip</label>
	//             <input type="text" required placeholder="Zip" class="form-control is-invalid" id="validationServer05">
	//                 <div class="invalid-feedback">Please provide a valid zip.</div>
	//         </div>
	//     </div>
	//     <div class="form-group">
	//         <div class="form-check">
	//             <input type="checkbox" required class="form-check-input is-invalid" id="invalidCheck3">
	//             <label for="invalidCheck3" class="form-check-label">Agree to terms and conditions</label>
	//             <div class="invalid-feedback">You must agree before submitting.</div>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_validation_custom_styles_1.0_text-_method">
	//     <button type="submit" class="btn btn-primary">Submit form</button>
	// </form>
}
