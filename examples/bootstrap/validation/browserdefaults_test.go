package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#browser-defaults
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleValidationBrowserDefaults() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Validation Browser Defaults", "")

	// Add a form row
	row1 := inputGroup.AddBlock("div", "")
	row1.ClassAdd("form-row")

	// Add a first name input
	fName := row1.Inputs.AddText("", "First name", "")
	fName.ID = "validationDefault01"
	fName.Placeholder = "First name"
	fName.Value = "Mark"
	fName.Required = true
	fName.FormGroup.OverwriteCSS = true
	fName.FormGroup.ClassAdd("col-md-4")
	fName.FormGroup.ClassAdd("mb-3")

	// Add a first name input
	lName := row1.Inputs.AddText("", "Last name", "")
	lName.ID = "validationDefault02"
	lName.Placeholder = "Last name"
	lName.Value = "Otto"
	lName.Required = true
	lName.FormGroup.OverwriteCSS = true
	lName.FormGroup.ClassAdd("col-md-4")
	lName.FormGroup.ClassAdd("mb-3")

	// Add an Username input
	username := row1.Inputs.AddText("", "Username", "")
	username.ID = "validationDefaultUsername"
	username.Placeholder = "Username"
	username.Required = true
	username.AttributeAdd("aria-describedby", "inputGroupPrepend2")
	username.FormGroup.OverwriteCSS = true
	username.FormGroup.ClassAdd("col-md-4")
	username.FormGroup.ClassAdd("mb-3")
	_, usernameText := username.InputGroup.Prepend("div", "span", "@")
	usernameText.ID = "inputGroupPrepend2"

	// Add a form row
	row2 := inputGroup.AddBlock("div", "")
	row2.ClassAdd("form-row")

	// Add a city input
	city := row2.Inputs.AddText("", "City", "")
	city.ID = "validationDefault03"
	city.Placeholder = "City"
	city.Required = true
	city.FormGroup.OverwriteCSS = true
	city.FormGroup.ClassAdd("col-md-6")
	city.FormGroup.ClassAdd("mb-3")

	// add a state input
	state := row2.Inputs.AddText("", "State", "")
	state.ID = "validationDefault04"
	state.Placeholder = "State"
	state.Required = true
	state.FormGroup.OverwriteCSS = true
	state.FormGroup.ClassAdd("col-md-3")
	state.FormGroup.ClassAdd("mb-3")

	// add a zipcode input
	zip := row2.Inputs.AddText("", "Zip", "")
	zip.ID = "validationDefault05"
	zip.Placeholder = "Zip"
	zip.Required = true
	zip.FormGroup.OverwriteCSS = true
	zip.FormGroup.ClassAdd("col-md-3")
	zip.FormGroup.ClassAdd("mb-3")

	// Add a checkbox input
	ckfg := inputGroup.AddBlock("div", "")
	ckfg.ClassAdd("form-group")
	checkbox := ckfg.Inputs.AddCheckBox("", "Agree to terms and conditions", "")
	checkbox.ID = "invalidCheck2"
	checkbox.Required = true

	// Add submit Button
	testForm.AddSubmitButton("Submit form")

	// Render the form
	depends.Render("validationbrowserdefaults", testForm)
	// Output:
	// <form name="Validation Browser Defaults" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_validation_browser_defaults">
	//     <div class="form-row">
	//         <div class="col-md-4 mb-3">
	//             <label for="validationDefault01">First name</label>
	//             <input type="text" required value="Mark" placeholder="First name" class="form-control" id="validationDefault01">
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationDefault02">Last name</label>
	//             <input type="text" required value="Otto" placeholder="Last name" class="form-control" id="validationDefault02">
	//         </div>
	//         <div class="col-md-4 mb-3">
	//             <label for="validationDefaultUsername">Username</label>
	//             <div class="input-group">
	//                 <div class="input-group-prepend">
	//                     <span class="input-group-text" id="inputGroupPrepend2">@</span>
	//                 </div>
	//                 <input type="text" required placeholder="Username" class="form-control" id="validationDefaultUsername" aria-describedby="inputGroupPrepend2">
	//             </div>
	//         </div>
	//     </div>
	//     <div class="form-row">
	//         <div class="col-md-6 mb-3">
	//             <label for="validationDefault03">City</label>
	//             <input type="text" required placeholder="City" class="form-control" id="validationDefault03">
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationDefault04">State</label>
	//             <input type="text" required placeholder="State" class="form-control" id="validationDefault04">
	//         </div>
	//         <div class="col-md-3 mb-3">
	//             <label for="validationDefault05">Zip</label>
	//             <input type="text" required placeholder="Zip" class="form-control" id="validationDefault05">
	//         </div>
	//     </div>
	//     <div class="form-group">
	//         <div class="form-check">
	//             <input type="checkbox" required class="form-check-input" id="invalidCheck2">
	//             <label for="invalidCheck2" class="form-check-label">Agree to terms and conditions</label>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_validation_browser_defaults_1.0_text-_method">
	//     <button type="submit" class="btn btn-primary">Submit form</button>
	// </form>
}
