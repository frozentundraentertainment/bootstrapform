package readonly

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#readonly
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleReadonly() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Read Only", "")

	// Add a read only text input
	textLg := inputGroup.AddText("", "", "")
	textLg.ReadOnly = true
	textLg.Placeholder = "Readonly input here..."

	// Render the form
	depends.Render("readonly", testForm)
	// Output:
	// <form name="Read Only" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_read_only">
	//     <input type="text" readonly placeholder="Readonly input here..." class="form-control">
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_read_only_1.0_text-_method">
	// </form>
}
