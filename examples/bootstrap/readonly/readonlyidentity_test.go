package readonly

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#readonly-plain-text
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleReadonlyIdentity() {

	// Identity Form
	testForm, inputGroup := bootstrapform.NewForm("Identity Form", "")
	testForm.ClassAdd("form-inline")

	identityEmail := inputGroup.AddEmail("email.address", "Email", "")
	identityEmail.FormGroup.ClassAdd("mb-2")
	identityEmail.Label.ClassAdd("sr-only")
	identityEmail.ID = "staticEmail2"
	identityEmail.ReadOnly = true
	identityEmail.ClassAdd("form-control-plaintext")
	identityEmail.Value = "email@example.com"

	identityPassword := inputGroup.AddPassword("password", "Password", "")
	identityPassword.FormGroup.ClassAdd("mx-sm-3")
	identityPassword.FormGroup.ClassAdd("mb-2")
	identityPassword.Label.ClassAdd("sr-only")
	identityPassword.ID = "inputPassword2"
	identityPassword.Placeholder = "Password"

	identitySubmit := testForm.AddSubmitButton("Confirm identity")
	identitySubmit.ClassAdd("mb-2")

	// Render the form
	depends.Render("readonlyidentity", testForm)
	// Output:
	// <form name="Identity Form" target="_self" method="post" enctype="application/x-www-form-urlencoded" class="form-inline" id="form_identity_form">
	//     <div class="mb-2 form-group">
	//         <label for="staticEmail2" class="sr-only">Email</label>
	//         <input type="email" name="email.address" readonly value="email@example.com" class="form-control-plaintext form-control" id="staticEmail2">
	//     </div>
	//     <div class="mx-sm-3 mb-2 form-group">
	//         <label for="inputPassword2" class="sr-only">Password</label>
	//         <input type="password" name="password" placeholder="Password" class="form-control" id="inputPassword2">
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_identity_form_1.0_text-_method">
	//     <button type="submit" class="mb-2 btn btn-primary">Confirm identity</button>
	// </form>
}
