package readonly

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
	"gitlab.com/frozentundraentertainment/bootstrapform/inputs"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#readonly-plain-text
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleReadonlyPlainText() {

	// Create the read only plain text form group
	testForm, inputGroup := bootstrapform.NewForm("Plain Text", "")

	// Add a read only plain text email input
	roEmail := inputGroup.AddEmail("email.address", "Email", "")
	roEmail.FormGroup.ClassAdd("row")
	roEmail.Label.ClassAdd("col-sm-2")
	roEmail.Label.ClassAdd("col-form-label")
	roEmail.ID = "staticEmail"
	roEmail.Value = "email@example.com"
	roEmail.ClassAdd("form-control-plaintext")
	roEmail.Wrap.Type = inputs.WrapDiv
	roEmail.Wrap.AddClass("col-sm-10")
	roEmail.ReadOnly = true

	// Add a password input
	roPass := inputGroup.AddPassword("password", "Password", "")
	roPass.FormGroup.ClassAdd("row")
	roPass.Label.ClassAdd("col-sm-2")
	roPass.Label.ClassAdd("col-form-label")
	roPass.ID = "inputPassword"
	roPass.Placeholder = "Password"
	roPass.Wrap.Type = inputs.WrapDiv
	roPass.Wrap.AddClass("col-sm-10")

	// Render the form
	depends.Render("readonlyplaintext", testForm)
	// Output:
	// <form name="Plain Text" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_plain_text">
	//     <div class="row form-group">
	//         <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
	//         <div class="col-sm-10">
	//             <input type="email" name="email.address" readonly value="email@example.com" class="form-control-plaintext form-control" id="staticEmail">
	//         </div>
	//     </div>
	//     <div class="row form-group">
	//         <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
	//         <div class="col-sm-10">
	//             <input type="password" name="password" placeholder="Password" class="form-control" id="inputPassword">
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_plain_text_1.0_text-_method">
	// </form>
}
