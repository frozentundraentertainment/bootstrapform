package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#form-controls
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleFormControls() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Form Controls", "")

	// Add an email input
	email := inputGroup.AddEmail("email.address", "Email address", "")
	email.ID = "exampleFormControlInput1"
	email.Placeholder = "name@example.com"

	// Add a select input
	option := inputGroup.AddSelect("single.select", "Example select", "")
	option.ID = "exampleFormControlSelect1"
	option.AddOption("1", false)
	option.AddOption("2", false)
	option.AddOption("3", false)
	option.AddOption("4", false)
	option.AddOption("5", false)

	// Add a multiple select input
	mOption := inputGroup.AddSelect("multi select", "Example multiple select", "")
	mOption.ID = "exampleFormControlSelect2"
	mOption.Multiple = true
	mOption.AddOption("1", false)
	mOption.AddOption("2", false)
	mOption.AddOption("3", false)
	mOption.AddOption("4", false)
	mOption.AddOption("5", false)

	// Add a text area input
	textArea := inputGroup.AddTextArea("text.area", "Example textarea", "")
	textArea.ID = "exampleFormControlTextarea1"
	textArea.Rows = 3

	// Add a File Upload input
	fileButton := inputGroup.AddFile("file.input", "Example File Upload", "")
	fileButton.ID = "exampleFormControlFile1"

	// Render the form
	depends.Render("formcontrols", testForm)
	// Output:
	// <form name="Form Controls" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_form_controls">
	//     <div class="form-group">
	//         <label for="exampleFormControlInput1">Email address</label>
	//         <input type="email" name="email.address" placeholder="name@example.com" class="form-control" id="exampleFormControlInput1">
	//     </div>
	//     <div class="form-group">
	//         <label for="exampleFormControlSelect1">Example select</label>
	//         <select name="single.select" class="form-control" id="exampleFormControlSelect1">
	//             <option>1</option>
	//             <option>2</option>
	//             <option>3</option>
	//             <option>4</option>
	//             <option>5</option>
	//         </select>
	//     </div>
	//     <div class="form-group">
	//         <label for="exampleFormControlSelect2">Example multiple select</label>
	//         <select name="multi select" class="form-control" id="exampleFormControlSelect2" multiple>
	//             <option>1</option>
	//             <option>2</option>
	//             <option>3</option>
	//             <option>4</option>
	//             <option>5</option>
	//         </select>
	//     </div>
	//     <div class="form-group">
	//         <label for="exampleFormControlTextarea1">Example textarea</label>
	//         <textarea name="text.area" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
	//     </div>
	//     <div class="form-group">
	//         <label for="exampleFormControlFile1">Example File Upload</label>
	//         <input type="file" name="file.input" class="form-control-file" id="exampleFormControlFile1">
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_form_controls_1.0_text-_method">
	// </form>
}
