package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#overview
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleOverview() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Overview", "")

	// Add an email input
	email := inputGroup.AddEmail("email.address", "Email address", `We'll never share your email with anyone else.`)
	email.ID = "exampleInputEmail1"
	email.Placeholder = "Enter email"
	email.Hint.ID = "emailHelp"

	// Add a password input
	password := inputGroup.AddPassword("password", "Password", "")
	password.ID = "exampleInputPassword1"
	password.Placeholder = "Password"

	// Add a checkbox input
	checkbox := inputGroup.AddCheckBox("checkbox", "Check Me Out", "")
	checkbox.ID = "exampleCheck1"

	// Add submit Button
	testForm.AddSubmitButton("Submit")

	// Render the form
	depends.Render("overview", testForm)
	// Output:
	// <form name="Overview" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_overview">
	//     <div class="form-group">
	//         <label for="exampleInputEmail1">Email address</label>
	//         <input type="email" name="email.address" placeholder="Enter email" aria-describedby="emailHelp" class="form-control" id="exampleInputEmail1">
	//         <small class="form-text text-muted" id="emailHelp">We'll never share your email with anyone else.</small>
	//     </div>
	//     <div class="form-group">
	//         <label for="exampleInputPassword1">Password</label>
	//         <input type="password" name="password" placeholder="Password" class="form-control" id="exampleInputPassword1">
	//     </div>
	//     <div class="form-check">
	//         <input type="checkbox" name="checkbox" class="form-check-input" id="exampleCheck1">
	//         <label for="exampleCheck1" class="form-check-label">Check Me Out</label>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_overview_1.0_text-_method">
	//     <button type="submit" class="btn btn-primary">Submit</button>
	// </form>
}
