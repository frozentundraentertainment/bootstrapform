package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#sizing
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleSizing() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Sizing", "")

	// Add a large text input
	textLg := inputGroup.AddText("large.text", "", "")
	textLg.ClassAdd("form-control-lg")
	textLg.Placeholder = ".form-control-lg"

	// Add a default text input
	text := inputGroup.AddText("default.text", "", "")
	text.Placeholder = "Default input"

	// Add a small text input
	textSm := inputGroup.AddText("small.text", "", "")
	textSm.ClassAdd("form-control-sm")
	textSm.Placeholder = ".form-control-sm"

	// Add a large select input
	optionLg := inputGroup.AddSelect("large.select", "", "")
	optionLg.ClassAdd("form-control-lg")
	optionLg.AddOption("Large Select", false)

	// Add a default select input
	option := inputGroup.AddSelect("default.select", "", "")
	option.AddOption("Default input", false)

	// Add a small select input
	optionSm := inputGroup.AddSelect("small.select", "", "")
	optionSm.ClassAdd("form-control-sm")
	optionSm.AddOption("Small Select", false)

	// Render the form
	depends.Render("sizing", testForm)
	// Output:
	// <form name="Sizing" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_sizing">
	//     <input type="text" name="large.text" placeholder=".form-control-lg" class="form-control-lg form-control" id="form_sizing_0.0_text-large.text">
	//     <input type="text" name="default.text" placeholder="Default input" class="form-control" id="form_sizing_0.1_text-default.text">
	//     <input type="text" name="small.text" placeholder=".form-control-sm" class="form-control-sm form-control" id="form_sizing_0.2_text-small.text">
	//     <select name="large.select" class="form-control-lg form-control" id="form_sizing_0.3_select-large.select">
	//         <option>Large Select</option>
	//     </select>
	//     <select name="default.select" class="form-control" id="form_sizing_0.4_select-default.select">
	//         <option>Default input</option>
	//     </select>
	//     <select name="small.select" class="form-control-sm form-control" id="form_sizing_0.5_select-small.select">
	//         <option>Small Select</option>
	//     </select>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_sizing_1.0_text-_method">
	// </form>
}
