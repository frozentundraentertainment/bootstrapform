package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#disabled-forms
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleDisabled() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Disabled", "")

	// Add a disabled fieldset
	fieldSet := inputGroup.AddFieldSet("", "")
	fieldSet.Disabled = true

	// Add a disabled text
	text := fieldSet.Inputs.AddText("", "Disabled input", "")
	text.ID = "disabledTextInput"
	text.Placeholder = "Disabled input"

	// Add a disabled select
	menu := fieldSet.Inputs.AddSelect("", "Disabled select menu", "")
	menu.ID = "disabledSelect"
	menu.AddOption("Disabled select", false)

	// Add a disabled check
	ck := fieldSet.Inputs.AddCheckBox("", "Can't check this", "")
	ck.ID = "disabledFieldsetCheck"
	ck.Disabled = true

	// Add submit Button
	fieldSet.Inputs.AddSubmit("", "Submit")

	// Render the form
	depends.Render("disabled", testForm)
	// Output:
	// <form name="Disabled" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_disabled">
	//     <fieldset disabled>
	//         <div class="form-group">
	//             <label for="disabledTextInput">Disabled input</label>
	//             <input type="text" placeholder="Disabled input" class="form-control" id="disabledTextInput">
	//         </div>
	//         <div class="form-group">
	//             <label for="disabledSelect">Disabled select menu</label>
	//             <select class="form-control" id="disabledSelect">
	//                 <option>Disabled select</option>
	//             </select>
	//         </div>
	//         <div class="form-check">
	//             <input type="checkbox" disabled class="form-check-input" id="disabledFieldsetCheck">
	//             <label for="disabledFieldsetCheck" class="form-check-label">Can't check this</label>
	//         </div>
	//         <button type="submit" class="btn btn-primary">Submit</button>
	//     </fieldset>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_disabled_1.0_text-_method">
	// </form>
}
