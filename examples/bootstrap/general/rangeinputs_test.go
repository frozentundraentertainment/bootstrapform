package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#range-inputs
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleRangeInputs() {

	// Identity Form
	testForm, inputGroup := bootstrapform.NewForm("Range Inputs", "")

	rangeInput := inputGroup.AddRange("number.range", "Example Range input", "")
	rangeInput.ID = "formControlRange"

	// Render the form
	depends.Render("rangeinputs", testForm)
	// Output:
	// <form name="Range Inputs" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_range_inputs">
	//     <div class="form-group">
	//         <label for="formControlRange">Example Range input</label>
	//         <input type="range" name="number.range" class="form-control-range" id="formControlRange">
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_range_inputs_1.0_text-_method">
	// </form>
}
