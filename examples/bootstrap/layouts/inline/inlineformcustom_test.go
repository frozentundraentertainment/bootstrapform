package inline

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#inline-forms
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleInlineFormCustom() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Inline Form Custom", "")
	testForm.ClassAdd("form-inline")

	// Add an inline preference input
	preference := inputGroup.AddSelect("", "Preference", "")
	preference.ID = "inlineFormCustomSelectPref"
	preference.OverwriteCSS = true
	preference.ClassAdd("custom-select")
	preference.ClassAdd("my-1")
	preference.ClassAdd("mr-sm-2")
	preference.Label.ClassAdd("my-1")
	preference.Label.ClassAdd("mr-2")
	preference.FormGroup.Enable = false
	preference.AddOption("Choose...", true)
	preference.AddOption("One", false).Value = "1"
	preference.AddOption("Two", false).Value = "2"
	preference.AddOption("Three", false).Value = "3"

	// Add an inline checkbox input
	remember := inputGroup.AddCheckBox("", "Remember my preference", "")
	remember.ID = "customControlInline"
	remember.BootstrapCustom = true
	remember.FormGroup.ClassAdd("my-1")
	remember.FormGroup.ClassAdd("mr-sm-2")

	// Add a submit button
	submit := inputGroup.AddSubmit("", "Submit")
	submit.ClassAdd("my-1")

	// Render the form
	depends.Render("inlineformcustom", testForm)
	// Output:
	// <form name="Inline Form Custom" target="_self" method="post" enctype="application/x-www-form-urlencoded" class="form-inline" id="form_inline_form_custom">
	//     <label for="inlineFormCustomSelectPref" class="my-1 mr-2">Preference</label>
	//     <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
	//         <option selected>Choose...</option>
	//         <option value="1">One</option>
	//         <option value="2">Two</option>
	//         <option value="3">Three</option>
	//     </select>
	//     <div class="my-1 mr-sm-2 custom-control custom-checkbox">
	//         <input type="checkbox" class="custom-control-input" id="customControlInline">
	//         <label for="customControlInline" class="custom-control-label">Remember my preference</label>
	//     </div>
	//     <button type="submit" class="my-1 btn btn-primary">Submit</button>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_inline_form_custom_1.0_text-_method">
	// </form>
}
