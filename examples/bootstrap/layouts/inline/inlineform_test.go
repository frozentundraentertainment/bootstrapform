package inline

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#inline-forms
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleInlineForm() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Inline Form", "")
	testForm.ClassAdd("form-inline")

	// Add an autosizing Name input
	name := inputGroup.AddText("", "Name", "")
	name.ID = "inlineFormInputName2"
	name.Placeholder = "Jane Doe"
	name.ClassAdd("mb-2")
	name.ClassAdd("mr-sm-2")
	name.Label.ClassAdd("sr-only")
	name.FormGroup.Enable = false

	// Add an autosizing Username input
	username := inputGroup.AddText("", "Username", "")
	username.ID = "inlineFormInputGroupUsername2"
	username.Placeholder = "Username"
	username.Label.ClassAdd("sr-only")
	username.FormGroup.Enable = false
	username.InputGroup.ClassAdd("mb-2")
	username.InputGroup.ClassAdd("mr-sm-2")
	username.InputGroup.Prepend("div", "div", "@")

	// Add a Remember me Checkbox
	remember := inputGroup.AddCheckBox("", "Remember me", "")
	remember.ID = "inlineFormCheck"
	remember.FormGroup.ClassAdd("mb-2")
	remember.FormGroup.ClassAdd("mr-sm-2")

	// Add a submit button
	submit := inputGroup.AddSubmit("", "Submit")
	submit.ClassAdd("mb-2")

	// Render the form
	depends.Render("inlineform", testForm)
	// Output:
	// <form name="Inline Form" target="_self" method="post" enctype="application/x-www-form-urlencoded" class="form-inline" id="form_inline_form">
	//     <label for="inlineFormInputName2" class="sr-only">Name</label>
	//     <input type="text" placeholder="Jane Doe" class="mb-2 mr-sm-2 form-control" id="inlineFormInputName2">
	//     <label for="inlineFormInputGroupUsername2" class="sr-only">Username</label>
	//     <div class="mb-2 mr-sm-2 input-group">
	//         <div class="input-group-prepend">
	//             <div class="input-group-text">@</div>
	//         </div>
	//         <input type="text" placeholder="Username" class="form-control" id="inlineFormInputGroupUsername2">
	//     </div>
	//     <div class="mb-2 mr-sm-2 form-check">
	//         <input type="checkbox" class="form-check-input" id="inlineFormCheck">
	//         <label for="inlineFormCheck" class="form-check-label">Remember me</label>
	//     </div>
	//     <button type="submit" class="mb-2 btn btn-primary">Submit</button>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_inline_form_1.0_text-_method">
	// </form>
}
