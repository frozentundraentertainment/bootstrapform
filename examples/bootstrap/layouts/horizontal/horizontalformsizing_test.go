package horizontal

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
	"gitlab.com/frozentundraentertainment/bootstrapform/inputs"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#horizontal-form-label-sizing
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleHorizontalFormSizing() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Horizontal Form Sizing", "")

	// Add an small email input
	emailsm := inputGroup.AddEmail("", "Email", "")
	emailsm.FormGroup.ClassAdd("row")
	emailsm.Label.OverwriteCSS = true
	emailsm.Label.ClassAdd("col-sm-2")
	emailsm.Label.ClassAdd("col-form-label")
	emailsm.Label.ClassAdd("col-form-label-sm")
	emailsm.Wrap.Type = inputs.WrapDiv
	emailsm.Wrap.AddClass("col-sm-10")
	emailsm.ID = "colFormLabelSm"
	emailsm.Placeholder = "col-form-label-sm"
	emailsm.ClassAdd("form-control-sm")

	// Add an email input
	email := inputGroup.AddEmail("", "Email", "")
	email.FormGroup.ClassAdd("row")
	email.Label.OverwriteCSS = true
	email.Label.ClassAdd("col-sm-2")
	email.Label.ClassAdd("col-form-label")
	email.Wrap.Type = inputs.WrapDiv
	email.Wrap.AddClass("col-sm-10")
	email.ID = "colFormLabel"
	email.Placeholder = "col-form-label"

	// Add an large email input
	emaillg := inputGroup.AddEmail("", "Email", "")
	emaillg.FormGroup.ClassAdd("row")
	emaillg.Label.OverwriteCSS = true
	emaillg.Label.ClassAdd("col-sm-2")
	emaillg.Label.ClassAdd("col-form-label")
	emaillg.Label.ClassAdd("col-form-label-lg")
	emaillg.Wrap.Type = inputs.WrapDiv
	emaillg.Wrap.AddClass("col-sm-10")
	emaillg.ID = "colFormLabelLg"
	emaillg.Placeholder = "col-form-label-lg"
	emaillg.ClassAdd("form-control-lg")

	// Render the form
	depends.Render("horizontalformsizing", testForm)
	// Output:
	// <form name="Horizontal Form Sizing" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_horizontal_form_sizing">
	//     <div class="row form-group">
	//         <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
	//         <div class="col-sm-10">
	//             <input type="email" placeholder="col-form-label-sm" class="form-control-sm form-control" id="colFormLabelSm">
	//         </div>
	//     </div>
	//     <div class="row form-group">
	//         <label for="colFormLabel" class="col-sm-2 col-form-label">Email</label>
	//         <div class="col-sm-10">
	//             <input type="email" placeholder="col-form-label" class="form-control" id="colFormLabel">
	//         </div>
	//     </div>
	//     <div class="row form-group">
	//         <label for="colFormLabelLg" class="col-sm-2 col-form-label col-form-label-lg">Email</label>
	//         <div class="col-sm-10">
	//             <input type="email" placeholder="col-form-label-lg" class="form-control-lg form-control" id="colFormLabelLg">
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_horizontal_form_sizing_1.0_text-_method">
	// </form>
}
