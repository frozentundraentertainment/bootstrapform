package horizontal

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#horizontal-form
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleHorizontalForm() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Horizontal Form", "")

	// Add an email input
	email := inputGroup.AddEmail("", "Email", "")
	email.ID = "inputEmail3"
	email.Placeholder = "Email"
	email.Wrap.AddClass("col-sm-10")
	email.Label.OverwriteCSS = true
	email.Label.ClassAdd("col-sm-2")
	email.Label.ClassAdd("col-form-label")
	email.FormGroup.ClassAdd("row")

	// Add a password input
	pass := inputGroup.AddPassword("", "Password", "")
	pass.ID = "inputPassword3"
	pass.Placeholder = "Password"
	pass.Wrap.AddClass("col-sm-10")
	pass.Label.OverwriteCSS = true
	pass.Label.ClassAdd("col-sm-2")
	pass.Label.ClassAdd("col-form-label")
	pass.FormGroup.ClassAdd("row")

	// Add a fieldset block
	fieldset := inputGroup.AddFieldSet("", "Radios")
	fieldset.ClassAdd("form-group")
	fieldset.InputWrap.AddClass("row")
	fieldset.Legend.ClassAdd("col-form-label")
	fieldset.Legend.ClassAdd("col-sm-2 pt-0")
	fieldset.Inputs.Wrap.AddClass("col-sm-10")

	// Add the first radio input
	radio1 := fieldset.Inputs.AddRadio("gridRadios", "First radio", "")
	radio1.ID = "gridRadios1"
	radio1.Value = "option1"
	radio1.Checked = true

	// Add the second radio input
	radio2 := fieldset.Inputs.AddRadio("gridRadios", "Second radio", "")
	radio2.ID = "gridRadios2"
	radio2.Value = "option2"

	// Add the third radio input
	radio3 := fieldset.Inputs.AddRadio("gridRadios", "Third disabled radio", "")
	radio3.ID = "gridRadios3"
	radio3.Value = "option3"
	radio3.Disabled = true
	radio3.FormGroup.ClassAdd("disabled")

	// Add a form group for the Checkbox
	checkIG := testForm.AddInputGroup()
	checkIG.FormGroup.Enable = true
	checkIG.FormGroup.ClassAdd("row")

	// Add the checkbox div
	checkDiv := checkIG.AddBlock("div", "Checkbox")
	checkDiv.ClassAdd("col-sm-2")

	// Add a checkbox wrapper
	checkWrap := checkIG.AddBlock("div", "")
	checkWrap.ClassAdd("col-sm-10")

	// Add the checkbox
	checkBox := checkWrap.Inputs.AddCheckBox("", "Example checkbox", "")
	checkBox.ID = "gridCheck1"

	// Add a group for the submit button for custom CSS
	submitIG := testForm.AddInputGroup()

	// Add a submit button
	submit := submitIG.AddSubmit("", "Sign in")
	submit.FormGroup.ClassAdd("row")
	submit.Wrap.AddClass("col-sm-10")

	// Render the form
	depends.Render("horizontalform", testForm)
	// Output:
	// <form name="Horizontal Form" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_horizontal_form">
	//     <div class="row form-group">
	//         <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
	//         <div class="col-sm-10">
	//             <input type="email" placeholder="Email" class="form-control" id="inputEmail3">
	//         </div>
	//     </div>
	//     <div class="row form-group">
	//         <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
	//         <div class="col-sm-10">
	//             <input type="password" placeholder="Password" class="form-control" id="inputPassword3">
	//         </div>
	//     </div>
	//     <fieldset class="form-group">
	//         <div class="row">
	//             <legend class="col-form-label col-sm-2 pt-0">Radios</legend>
	//             <div class="col-sm-10">
	//                 <div class="form-check">
	//                     <input type="radio" name="gridRadios" value="option1" class="form-check-input" id="gridRadios1" checked>
	//                     <label for="gridRadios1" class="form-check-label">First radio</label>
	//                 </div>
	//                 <div class="form-check">
	//                     <input type="radio" name="gridRadios" value="option2" class="form-check-input" id="gridRadios2">
	//                     <label for="gridRadios2" class="form-check-label">Second radio</label>
	//                 </div>
	//                 <div class="disabled form-check">
	//                     <input type="radio" disabled name="gridRadios" value="option3" class="form-check-input" id="gridRadios3">
	//                     <label for="gridRadios3" class="form-check-label">Third disabled radio</label>
	//                 </div>
	//             </div>
	//         </div>
	//     </fieldset>
	//     <div class="row form-group">
	//         <div class="col-sm-2">Checkbox</div>
	//         <div class="col-sm-10">
	//             <div class="form-check">
	//                 <input type="checkbox" class="form-check-input" id="gridCheck1">
	//                 <label for="gridCheck1" class="form-check-label">Example checkbox</label>
	//             </div>
	//         </div>
	//     </div>
	//     <div class="form-group row">
	//         <div class="col-sm-10">
	//             <button type="submit" class="btn btn-primary">Sign in</button>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_horizontal_form_3.0_text-_method">
	// </form>
}
