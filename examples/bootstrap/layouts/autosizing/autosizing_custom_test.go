package autosizing

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#auto-sizing
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleAutoSizingCustom() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Auto Sizing Custom", "")
	inputGroup.Wrap.AddClass("form-row")
	inputGroup.Wrap.AddClass("align-items-center")

	// Add an autosizing Preference input
	preference := inputGroup.AddSelect("", "Preference", "")
	preference.ID = "inlineFormCustomSelect"
	preference.BootstrapCustom = true
	preference.ClassAdd("mr-sm-2")
	preference.Label.OverwriteCSS = true
	preference.Label.ClassAdd("mr-sm-2")
	preference.Label.ClassAdd("sr-only")
	preference.FormGroup.OverwriteCSS = true
	preference.FormGroup.ClassAdd("col-auto")
	preference.FormGroup.ClassAdd("my-1")
	preference.AddOption("Choose...", true)
	preference.AddOption("One", false).Value = "1"
	preference.AddOption("Two", false).Value = "2"
	preference.AddOption("Three", false).Value = "3"

	// Add a Remember me Checkbox
	remember := inputGroup.AddCheckBox("", "Remember my preference", "")
	remember.ID = "customControlAutosizing"
	remember.BootstrapCustom = true
	remember.FormGroup.ClassAdd("mr-sm-2")
	remember.FormGroup.Wrap.AddClass("col-auto")
	remember.FormGroup.Wrap.AddClass("my-1")

	// Add a submit button
	submit := inputGroup.AddSubmit("", "Submit")
	submit.Wrap.AddClass("col-auto")
	submit.Wrap.AddClass("my-1")

	// Render the form
	depends.Render("autosizing_custom", testForm)
	// Output:
	// <form name="Auto Sizing Custom" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_auto_sizing_custom">
	//     <div class="form-row align-items-center">
	//         <div class="col-auto my-1">
	//             <label for="inlineFormCustomSelect" class="mr-sm-2 sr-only">Preference</label>
	//             <select class="mr-sm-2 custom-select" id="inlineFormCustomSelect">
	//                 <option selected>Choose...</option>
	//                 <option value="1">One</option>
	//                 <option value="2">Two</option>
	//                 <option value="3">Three</option>
	//             </select>
	//         </div>
	//         <div class="col-auto my-1">
	//             <div class="mr-sm-2 custom-control custom-checkbox">
	//                 <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
	//                 <label for="customControlAutosizing" class="custom-control-label">Remember my preference</label>
	//             </div>
	//         </div>
	//         <div class="col-auto my-1">
	//             <button type="submit" class="btn btn-primary">Submit</button>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_auto_sizing_custom_1.0_text-_method">
	// </form>
}
