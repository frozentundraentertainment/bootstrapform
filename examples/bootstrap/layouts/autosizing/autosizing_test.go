package autosizing

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#auto-sizing
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleAutoSizing() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Auto Sizing", "")
	inputGroup.Wrap.AddClass("form-row")
	inputGroup.Wrap.AddClass("align-items-center")

	// Add an autosizing Name input
	name := inputGroup.AddText("", "Name", "")
	name.ID = "inlineFormInput"
	name.Placeholder = "Jane Doe"
	name.ClassAdd("mb-2")
	name.Label.ClassAdd("sr-only")
	name.FormGroup.OverwriteCSS = true
	name.FormGroup.ClassAdd("col-auto")

	// Add an autosizing Username input
	username := inputGroup.AddText("", "Username", "")
	username.ID = "inlineFormInputGroup"
	username.Placeholder = "Username"
	username.Label.ClassAdd("sr-only")
	username.FormGroup.OverwriteCSS = true
	username.FormGroup.ClassAdd("col-auto")
	username.InputGroup.ClassAdd("mb-2")
	username.InputGroup.Prepend("div", "div", "@")

	// Add a Remember me Checkbox
	remember := inputGroup.AddCheckBox("", "Remember me", "")
	remember.ID = "autoSizingCheck"
	remember.FormGroup.ClassAdd("mb-2")
	remember.FormGroup.Wrap.AddClass("col-auto")

	// Add a submit button
	submit := inputGroup.AddSubmit("", "Submit")
	submit.ClassAdd("mb-2")
	submit.Wrap.AddClass("col-auto")

	// Render the form
	depends.Render("autosizing", testForm)
	// Output:
	// <form name="Auto Sizing" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_auto_sizing">
	//     <div class="form-row align-items-center">
	//         <div class="col-auto">
	//             <label for="inlineFormInput" class="sr-only">Name</label>
	//             <input type="text" placeholder="Jane Doe" class="mb-2 form-control" id="inlineFormInput">
	//         </div>
	//         <div class="col-auto">
	//             <label for="inlineFormInputGroup" class="sr-only">Username</label>
	//             <div class="mb-2 input-group">
	//                 <div class="input-group-prepend">
	//                     <div class="input-group-text">@</div>
	//                 </div>
	//                 <input type="text" placeholder="Username" class="form-control" id="inlineFormInputGroup">
	//             </div>
	//         </div>
	//         <div class="col-auto">
	//             <div class="mb-2 form-check">
	//                 <input type="checkbox" class="form-check-input" id="autoSizingCheck">
	//                 <label for="autoSizingCheck" class="form-check-label">Remember me</label>
	//             </div>
	//         </div>
	//         <div class="col-auto">
	//             <button type="submit" class="mb-2 btn btn-primary">Submit</button>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_auto_sizing_1.0_text-_method">
	// </form>
}
