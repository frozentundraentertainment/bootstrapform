package autosizing

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#auto-sizing
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleAutoSizingColumn() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Auto Sizing Column", "")
	inputGroup.Wrap.AddClass("form-row")
	inputGroup.Wrap.AddClass("align-items-center")

	// Add an autosizing Name input
	name := inputGroup.AddText("", "Name", "")
	name.ID = "inlineFormInputGroupUsername"
	name.Placeholder = "Jane Doe"
	name.Label.ClassAdd("sr-only")
	name.FormGroup.OverwriteCSS = true
	name.FormGroup.ClassAdd("col-sm-3")
	name.FormGroup.ClassAdd("my-1")

	// Add an autosizing Username input
	username := inputGroup.AddText("", "Username", "")
	username.ID = "inlineFormInputGroup"
	username.Placeholder = "Username"
	username.Label.ClassAdd("sr-only")
	username.FormGroup.OverwriteCSS = true
	username.FormGroup.ClassAdd("col-sm-3")
	username.FormGroup.ClassAdd("my-1")
	username.InputGroup.Prepend("div", "div", "@")

	// Add a Remember me Checkbox
	remember := inputGroup.AddCheckBox("", "Remember me", "")
	remember.ID = "autoSizingCheck2"
	remember.FormGroup.Wrap.AddClass("col-auto")
	remember.FormGroup.Wrap.AddClass("my-1")

	// Add a submit button
	submit := inputGroup.AddSubmit("", "Submit")
	submit.Wrap.AddClass("col-auto")
	submit.Wrap.AddClass("my-1")

	// Render the form
	depends.Render("autosizing_column", testForm)
	// Output:
	// <form name="Auto Sizing Column" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_auto_sizing_column">
	//     <div class="form-row align-items-center">
	//         <div class="col-sm-3 my-1">
	//             <label for="inlineFormInputGroupUsername" class="sr-only">Name</label>
	//             <input type="text" placeholder="Jane Doe" class="form-control" id="inlineFormInputGroupUsername">
	//         </div>
	//         <div class="col-sm-3 my-1">
	//             <label for="inlineFormInputGroup" class="sr-only">Username</label>
	//             <div class="input-group">
	//                 <div class="input-group-prepend">
	//                     <div class="input-group-text">@</div>
	//                 </div>
	//                 <input type="text" placeholder="Username" class="form-control" id="inlineFormInputGroup">
	//             </div>
	//         </div>
	//         <div class="col-auto my-1">
	//             <div class="form-check">
	//                 <input type="checkbox" class="form-check-input" id="autoSizingCheck2">
	//                 <label for="autoSizingCheck2" class="form-check-label">Remember me</label>
	//             </div>
	//         </div>
	//         <div class="col-auto my-1">
	//             <button type="submit" class="btn btn-primary">Submit</button>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_auto_sizing_column_1.0_text-_method">
	// </form>
}
