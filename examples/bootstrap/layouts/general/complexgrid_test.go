package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#form-row
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleComplexGrid() {
	// Create the form
	testForm, loginIG := bootstrapform.NewForm("Complex Grid", "")
	loginIG.Wrap.AddClass("form-row")

	// Add a email input
	email := loginIG.AddEmail("", "Email", "")
	email.Placeholder = "Email"
	email.ID = "InputEmail4"
	email.FormGroup.ClassAdd("col-md-6")

	// Add a password input
	password := loginIG.AddPassword("", "Password", "")
	password.Placeholder = "Password"
	password.ID = "InputPassword4"
	password.FormGroup.ClassAdd("col-md-6")

	// Street Address Input Group
	addressIG := testForm.AddInputGroup()

	// Add Address Line 1
	addy1 := addressIG.AddText("", "Address", "")
	addy1.ID = "inputAddress"
	addy1.Placeholder = "1234 Main St"

	// Add Address Line 2
	addy2 := addressIG.AddText("", "Address 2", "")
	addy2.ID = "inputAddress2"
	addy2.Placeholder = "Apartment, studio, or floor"

	// Locale Input Group
	localeIG := testForm.AddInputGroup()
	localeIG.Wrap.AddClass("form-row")

	// Add City Input
	city := localeIG.AddText("", "City", "")
	city.ID = "inputCity"
	city.FormGroup.ClassAdd("col-md-6")

	// Add State Input
	state := localeIG.AddSelect("", "State", "")
	state.ID = "inputState"
	state.FormGroup.ClassAdd("col-md-4")
	state.AddOption("Choose...", true)
	state.AddOption("...", false)

	// Add Zip Code input
	zip := localeIG.AddText("", "Zip", "")
	zip.ID = "inputZip"
	zip.FormGroup.ClassAdd("col-md-2")

	// Add Checkbox Input Group
	checkIG := testForm.AddInputGroup()
	checkIG.FormGroup.Enable = true

	// Add Checkbox Input
	checkbox := checkIG.AddCheckBox("", "Check me out", "")
	checkbox.ID = "gridCheck"

	// Add submit button
	testForm.AddSubmitButton("Sign in")

	// Render the form
	depends.Render("complexgrid", testForm)
	// Output:
	// <form name="Complex Grid" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_complex_grid">
	//     <div class="form-row">
	//         <div class="col-md-6 form-group">
	//             <label for="InputEmail4">Email</label>
	//             <input type="email" placeholder="Email" class="form-control" id="InputEmail4">
	//         </div>
	//         <div class="col-md-6 form-group">
	//             <label for="InputPassword4">Password</label>
	//             <input type="password" placeholder="Password" class="form-control" id="InputPassword4">
	//         </div>
	//     </div>
	//     <div class="form-group">
	//         <label for="inputAddress">Address</label>
	//         <input type="text" placeholder="1234 Main St" class="form-control" id="inputAddress">
	//     </div>
	//     <div class="form-group">
	//         <label for="inputAddress2">Address 2</label>
	//         <input type="text" placeholder="Apartment, studio, or floor" class="form-control" id="inputAddress2">
	//     </div>
	//     <div class="form-row">
	//         <div class="col-md-6 form-group">
	//             <label for="inputCity">City</label>
	//             <input type="text" class="form-control" id="inputCity">
	//         </div>
	//         <div class="col-md-4 form-group">
	//             <label for="inputState">State</label>
	//             <select class="form-control" id="inputState">
	//                 <option selected>Choose...</option>
	//                 <option>...</option>
	//             </select>
	//         </div>
	//         <div class="col-md-2 form-group">
	//             <label for="inputZip">Zip</label>
	//             <input type="text" class="form-control" id="inputZip">
	//         </div>
	//     </div>
	//     <div class="form-group">
	//         <div class="form-check">
	//             <input type="checkbox" class="form-check-input" id="gridCheck">
	//             <label for="gridCheck" class="form-check-label">Check me out</label>
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_complex_grid_4.0_text-_method">
	//     <button type="submit" class="btn btn-primary">Sign in</button>
	// </form>
}
