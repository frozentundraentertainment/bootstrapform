package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
	"gitlab.com/frozentundraentertainment/bootstrapform/inputs"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#column-sizing
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleColumnSizing() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Column Sizing", "")

	// Wrap the Input Group
	inputGroup.Wrap.AddClass("form-row")

	// Add a "City" input
	city := inputGroup.AddText("", "", "")
	city.Placeholder = "City"
	city.Wrap.Type = inputs.WrapDiv
	city.Wrap.AddClass("col-7")

	// Add a "State" input
	state := inputGroup.AddText("", "", "")
	state.Placeholder = "State"
	state.Wrap.Type = inputs.WrapDiv
	state.Wrap.AddClass("col")

	// Add a "Zip" input
	zip := inputGroup.AddText("", "", "")
	zip.Placeholder = "Zip"
	zip.Wrap.Type = inputs.WrapDiv
	zip.Wrap.AddClass("col")

	// Render the form
	depends.Render("columnsizing", testForm)
	// Output:
	// <form name="Column Sizing" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_column_sizing">
	//     <div class="form-row">
	//         <div class="col-7">
	//             <input type="text" placeholder="City" class="form-control">
	//         </div>
	//         <div class="col">
	//             <input type="text" placeholder="State" class="form-control">
	//         </div>
	//         <div class="col">
	//             <input type="text" placeholder="Zip" class="form-control">
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_column_sizing_1.0_text-_method">
	// </form>
}
