package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#form-groups
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleFormGroups() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Form Groups", "")

	// Add a text input
	text1 := inputGroup.AddText("", "Example label", "")
	text1.ID = "formGroupExampleInput"
	text1.Placeholder = "Example input"

	// Add a text input
	text2 := inputGroup.AddText("", "Another label", "")
	text2.ID = "formGroupExampleInput2"
	text2.Placeholder = "Another input"

	// Render the form
	depends.Render("formgroups", testForm)
	// Output:
	// <form name="Form Groups" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_form_groups">
	//     <div class="form-group">
	//         <label for="formGroupExampleInput">Example label</label>
	//         <input type="text" placeholder="Example input" class="form-control" id="formGroupExampleInput">
	//     </div>
	//     <div class="form-group">
	//         <label for="formGroupExampleInput2">Another label</label>
	//         <input type="text" placeholder="Another input" class="form-control" id="formGroupExampleInput2">
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_form_groups_1.0_text-_method">
	// </form>
}
