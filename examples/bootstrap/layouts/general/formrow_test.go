package general

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#form-row
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleFormRow() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Form Row", "")
	inputGroup.Wrap.AddClass("form-row")

	// Add a text input
	text1 := inputGroup.AddText("", "", "")
	text1.Placeholder = "First name"
	text1.Wrap.AddClass("col")

	// Add a text input
	text2 := inputGroup.AddText("", "", "")
	text2.Placeholder = "Last name"
	text2.Wrap.AddClass("col")

	// Render the form
	depends.Render("formrow", testForm)
	// Output:
	// <form name="Form Row" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_form_row">
	//     <div class="form-row">
	//         <div class="col">
	//             <input type="text" placeholder="First name" class="form-control">
	//         </div>
	//         <div class="col">
	//             <input type="text" placeholder="Last name" class="form-control">
	//         </div>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_form_row_1.0_text-_method">
	// </form>
}
