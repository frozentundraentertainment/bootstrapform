package custom

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

func ExampleFormOptions() {

	// Create a blank form
	testForm, inputGroup := bootstrapform.NewForm("Form Options", "")
	testForm.ID = "NewID"
	testForm.SetIndentLevel(1)
	testForm.SetIndentCharacter("____")

	check1 := inputGroup.AddCheckBox("", "1", "")
	check1.FormGroup.ClassAdd("form-check-inline")
	check1.ID = "inlineCheckbox1"
	check1.Value = "option1"

	check2 := inputGroup.AddCheckBox("", "2", "")
	check2.FormGroup.ClassAdd("form-check-inline")
	check2.ID = "inlineCheckbox2"
	check2.Value = "option2"

	check3 := inputGroup.AddCheckBox("", "3 (disabled)", "")
	check3.FormGroup.ClassAdd("form-check-inline")
	check3.ID = "inlineCheckbox3"
	check3.Value = "option3"
	check3.Disabled = true

	// Render the form
	depends.Render("formoptions", testForm)
	// Output:
	// ____<form name="Form Options" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="NewID">
	// ________<div class="form-check-inline form-check">
	// ____________<input type="checkbox" value="option1" class="form-check-input" id="inlineCheckbox1">
	// ____________<label for="inlineCheckbox1" class="form-check-label">1</label>
	// ________</div>
	// ________<div class="form-check-inline form-check">
	// ____________<input type="checkbox" value="option2" class="form-check-input" id="inlineCheckbox2">
	// ____________<label for="inlineCheckbox2" class="form-check-label">2</label>
	// ________</div>
	// ________<div class="form-check-inline form-check">
	// ____________<input type="checkbox" disabled value="option3" class="form-check-input" id="inlineCheckbox3">
	// ____________<label for="inlineCheckbox3" class="form-check-label">3 (disabled)</label>
	// ________</div>
	// ________<input type="text" name="_method" readonly value="POST" class="form-control" hidden id="NewID_1.0_text-_method">
	// ____</form>
}
