package custom

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
	"gitlab.com/frozentundraentertainment/bootstrapform/inputs"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#custom-forms
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleCustomForms() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Custom Forms", "")

	// Add a checkbox
	checkbox1 := inputGroup.AddCheckBox("", "Check this custom checkbox", "")
	checkbox1.ID = "customCheck1"
	checkbox1.BootstrapCustom = true

	// Add a custom radio
	radio1 := inputGroup.AddRadio("customRadio", "Toggle this custom radio", "")
	radio1.ID = "customRadio1"
	radio1.BootstrapCustom = true

	// Add a custom radio
	radio2 := inputGroup.AddRadio("customRadio", "Or toggle this other custom radio", "")
	radio2.ID = "customRadio2"
	radio2.BootstrapCustom = true

	// Add a custom radio
	radio3 := inputGroup.AddRadio("customRadioInline1", "Toggle this custom radio", "")
	radio3.ID = "customRadioInline1"
	radio3.BootstrapCustom = true
	radio3.Inline = true

	// Add a custom radio
	radio4 := inputGroup.AddRadio("customRadioInline1", "Or toggle this other custom radio", "")
	radio4.ID = "customRadioInline2"
	radio4.BootstrapCustom = true
	radio4.Inline = true

	// Add a checkbox
	checkbox2 := inputGroup.AddCheckBox("", "Check this custom checkbox", "")
	checkbox2.ID = "customCheck1Disabled"
	checkbox2.Disabled = true
	checkbox2.BootstrapCustom = true

	// Add a custom radio
	radio5 := inputGroup.AddRadio("radioDisabled", "Toggle this custom radio", "")
	radio5.ID = "customRadioDisabled2"
	radio5.Disabled = true
	radio5.BootstrapCustom = true

	// Add a switch
	switch1 := inputGroup.AddCheckBox("", "Toggle this switch element", "")
	switch1.ID = "customSwitch1"
	switch1.OverwriteCSS = true
	switch1.ClassAdd("custom-control-input")
	switch1.FormGroup.OverwriteCSS = true
	switch1.FormGroup.ClassAdd("custom-control")
	switch1.FormGroup.ClassAdd("custom-switch")
	switch1.Label.OverwriteCSS = true
	switch1.Label.ClassAdd("custom-control-label")

	// Add a switch
	switch2 := inputGroup.AddCheckBox("", "Disabled switch element", "")
	switch2.ID = "customSwitch2"
	switch2.OverwriteCSS = true
	switch2.Disabled = true
	switch2.ClassAdd("custom-control-input")
	switch2.FormGroup.OverwriteCSS = true
	switch2.FormGroup.ClassAdd("custom-control")
	switch2.FormGroup.ClassAdd("custom-switch")
	switch2.Label.OverwriteCSS = true
	switch2.Label.ClassAdd("custom-control-label")

	// Select Menu
	select1 := inputGroup.AddSelect("", "", "")
	select1.BootstrapCustom = true
	select1.AddOption("Open this select menu", true)
	select1.AddOption("One", false).Value = "1"
	select1.AddOption("Two", false).Value = "2"
	select1.AddOption("Three", false).Value = "3"

	// Select Menu
	select2 := inputGroup.AddSelect("", "", "")
	select2.BootstrapCustom = true
	select2.ClassAdd("custom-select-lg")
	select2.ClassAdd("mb-3")
	select2.AddOption("Open this select menu", true)
	select2.AddOption("One", false).Value = "1"
	select2.AddOption("Two", false).Value = "2"
	select2.AddOption("Three", false).Value = "3"

	// Select Menu
	select3 := inputGroup.AddSelect("", "", "")
	select3.BootstrapCustom = true
	select3.ClassAdd("custom-select-sm")
	select3.AddOption("Open this select menu", true)
	select3.AddOption("One", false).Value = "1"
	select3.AddOption("Two", false).Value = "2"
	select3.AddOption("Three", false).Value = "3"

	// Select Menu
	select4 := inputGroup.AddSelect("", "", "")
	select4.BootstrapCustom = true
	select4.Multiple = true
	select4.AddOption("Open this select menu", true)
	select4.AddOption("One", false).Value = "1"
	select4.AddOption("Two", false).Value = "2"
	select4.AddOption("Three", false).Value = "3"

	// Select Menu
	select5 := inputGroup.AddSelect("", "", "")
	select5.BootstrapCustom = true
	select5.Size = 3
	select5.AddOption("Open this select menu", true)
	select5.AddOption("One", false).Value = "1"
	select5.AddOption("Two", false).Value = "2"
	select5.AddOption("Three", false).Value = "3"

	// Add a Range input
	range1 := inputGroup.AddRange("", "Example range", "")
	range1.ID = "customRange1"
	range1.BootstrapCustom = true
	range1.Label.OverwriteCSS = true
	range1.FormGroup.Enable = false

	// Add a Range input
	range2 := inputGroup.AddRange("", "Example range", "")
	range2.ID = "customRange2"
	range2.Min = 0
	range2.Max = 5
	range2.BootstrapCustom = true
	range2.Label.OverwriteCSS = true
	range2.FormGroup.Enable = false

	// Add a Range input
	range3 := inputGroup.AddRange("", "Example range", "")
	range3.ID = "customRange3"
	range3.Min = 0
	range3.Max = 5
	range3.Step = .5
	range3.BootstrapCustom = true
	range3.Label.OverwriteCSS = true
	range3.FormGroup.Enable = false

	// Add a file input
	file := inputGroup.AddFile("", "Choose file", "")
	file.ID = "customFile"
	file.BootstrapCustom = true
	file.OverwriteCSS = true
	file.ClassAdd("custom-file-input")
	file.Order = inputs.OrderInputLabelHint

	// Render the form
	depends.Render("customforms", testForm)
	// Output:
	// <form name="Custom Forms" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_custom_forms">
	//     <div class="custom-control custom-checkbox">
	//         <input type="checkbox" class="custom-control-input" id="customCheck1">
	//         <label for="customCheck1" class="custom-control-label">Check this custom checkbox</label>
	//     </div>
	//     <div class="custom-control custom-radio">
	//         <input type="radio" name="customRadio" class="custom-control-input" id="customRadio1">
	//         <label for="customRadio1" class="custom-control-label">Toggle this custom radio</label>
	//     </div>
	//     <div class="custom-control custom-radio">
	//         <input type="radio" name="customRadio" class="custom-control-input" id="customRadio2">
	//         <label for="customRadio2" class="custom-control-label">Or toggle this other custom radio</label>
	//     </div>
	//     <div class="custom-control-inline custom-control custom-radio">
	//         <input type="radio" name="customRadioInline1" class="custom-control-input" id="customRadioInline1">
	//         <label for="customRadioInline1" class="custom-control-label">Toggle this custom radio</label>
	//     </div>
	//     <div class="custom-control-inline custom-control custom-radio">
	//         <input type="radio" name="customRadioInline1" class="custom-control-input" id="customRadioInline2">
	//         <label for="customRadioInline2" class="custom-control-label">Or toggle this other custom radio</label>
	//     </div>
	//     <div class="custom-control custom-checkbox">
	//         <input type="checkbox" disabled class="custom-control-input" id="customCheck1Disabled">
	//         <label for="customCheck1Disabled" class="custom-control-label">Check this custom checkbox</label>
	//     </div>
	//     <div class="custom-control custom-radio">
	//         <input type="radio" disabled name="radioDisabled" class="custom-control-input" id="customRadioDisabled2">
	//         <label for="customRadioDisabled2" class="custom-control-label">Toggle this custom radio</label>
	//     </div>
	//     <div class="custom-control custom-switch">
	//         <input type="checkbox" class="custom-control-input" id="customSwitch1">
	//         <label for="customSwitch1" class="custom-control-label">Toggle this switch element</label>
	//     </div>
	//     <div class="custom-control custom-switch">
	//         <input type="checkbox" disabled class="custom-control-input" id="customSwitch2">
	//         <label for="customSwitch2" class="custom-control-label">Disabled switch element</label>
	//     </div>
	//     <select class="custom-select">
	//         <option selected>Open this select menu</option>
	//         <option value="1">One</option>
	//         <option value="2">Two</option>
	//         <option value="3">Three</option>
	//     </select>
	//     <select class="custom-select-lg mb-3 custom-select">
	//         <option selected>Open this select menu</option>
	//         <option value="1">One</option>
	//         <option value="2">Two</option>
	//         <option value="3">Three</option>
	//     </select>
	//     <select class="custom-select-sm custom-select">
	//         <option selected>Open this select menu</option>
	//         <option value="1">One</option>
	//         <option value="2">Two</option>
	//         <option value="3">Three</option>
	//     </select>
	//     <select class="custom-select" multiple>
	//         <option selected>Open this select menu</option>
	//         <option value="1">One</option>
	//         <option value="2">Two</option>
	//         <option value="3">Three</option>
	//     </select>
	//     <select class="custom-select" size="3">
	//         <option selected>Open this select menu</option>
	//         <option value="1">One</option>
	//         <option value="2">Two</option>
	//         <option value="3">Three</option>
	//     </select>
	//     <label for="customRange1">Example range</label>
	//     <input type="range" class="custom-range" id="customRange1">
	//     <label for="customRange2">Example range</label>
	//     <input type="range" class="custom-range" id="customRange2" max="5">
	//     <label for="customRange3">Example range</label>
	//     <input type="range" class="custom-range" id="customRange3" max="5" step="0.5">
	//     <div class="custom-file">
	//         <input type="file" class="custom-file-input" id="customFile">
	//         <label for="customFile" class="custom-file-label">Choose file</label>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_custom_forms_1.0_text-_method">
	// </form>
}
