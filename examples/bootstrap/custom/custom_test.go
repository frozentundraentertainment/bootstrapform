package custom

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
	"gitlab.com/frozentundraentertainment/bootstrapform/types"
)

func ExampleCustom() {
	// Create the form
	testForm, inputGroup := bootstrapform.NewForm("Test", "/formsubmit")
	testForm.Method = types.MethodPost
	testForm.EncType = types.EncodeURL
	testForm.AutoComplete = true
	testForm.NoValidate = true

	// Add an input
	inputGroup.FormGroup.Enable = true
	inputGroup.FormGroup.ID = "login"
	inputGroup.AddEmail("user.email", "Enter your email address:", "This will be your username")
	inputGroup.AddText("user.firstname", "Enter your first name:", "Only your first name")
	inputGroup.AddText("user.lastname", "Enter your last name:", "")

	// Add submit Button
	inputGroup.AddSubmit("submit", "Signup!")

	// Render the form
	depends.Render("custom", testForm)
	// Output:
	// <form name="Test" action="/formsubmit" target="_self" method="post" enctype="application/x-www-form-urlencoded" novalidate id="form_test">
	//     <div class="form-group" id="login">
	//         <div class="form-group">
	//             <label for="form_test_0.0_email-user.email">Enter your email address:</label>
	//             <input type="email" name="user.email" class="form-control" id="form_test_0.0_email-user.email">
	//             <small class="form-text text-muted">This will be your username</small>
	//         </div>
	//         <div class="form-group">
	//             <label for="form_test_0.1_text-user.firstname">Enter your first name:</label>
	//             <input type="text" name="user.firstname" class="form-control" id="form_test_0.1_text-user.firstname">
	//             <small class="form-text text-muted">Only your first name</small>
	//         </div>
	//         <div class="form-group">
	//             <label for="form_test_0.2_text-user.lastname">Enter your last name:</label>
	//             <input type="text" name="user.lastname" class="form-control" id="form_test_0.2_text-user.lastname">
	//         </div>
	//         <button name="submit" type="submit" class="btn btn-primary">Signup!</button>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_test_1.0_text-_method">
	// </form>
}
