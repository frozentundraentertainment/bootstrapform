package checkbox

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#without-labels
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleWithoutLabels() {

	// Create a blank form
	testForm, inputGroup := bootstrapform.NewForm("Without Labels", "")

	check := inputGroup.AddCheckBox("", "", "")
	check.FormGroup.ClassAdd("position-static")
	check.ID = "blankCheckbox"
	check.Value = "option1"
	check.EnableFGWithoutLabelHint = true
	// TODO: add ARIA Attribute
	// check.DataAdd("aria-label", "...")

	radio := inputGroup.AddRadio("blankRadio", "", "")
	radio.FormGroup.ClassAdd("position-static")
	radio.ID = "blankRadio1"
	radio.Value = "option1"
	radio.EnableFGWithoutLabelHint = true
	// TODO: add ARIA Attribute
	// radio.DataAdd("aria-label", "...")

	// Render the form
	depends.Render("withoutlables", testForm)
	// Output:
	// <form name="Without Labels" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_without_labels">
	//     <div class="position-static form-check">
	//         <input type="checkbox" value="option1" class="form-check-input" id="blankCheckbox">
	//     </div>
	//     <div class="position-static form-check">
	//         <input type="radio" name="blankRadio" value="option1" class="form-check-input" id="blankRadio1">
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_without_labels_1.0_text-_method">
	// </form>
}
