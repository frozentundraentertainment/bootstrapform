package checkbox

import (
	"gitlab.com/frozentundraentertainment/bootstrapform"
	"gitlab.com/frozentundraentertainment/bootstrapform/examples/bootstrap/depends"
)

// This will render the linked example Bootstrap form
// https://getbootstrap.com/docs/4.3/components/forms/#default-stacked
// It will be functionally the same other than some minor artifacts from the automation process.
func ExampleCheckStacked() {

	// Create a blank form
	testForm, inputGroup := bootstrapform.NewForm("Check Stacked", "")

	defaultCheck := inputGroup.AddCheckBox("", "Default Checkbox", "")
	defaultCheck.ID = "defaultCheck1"

	disabledCheck := inputGroup.AddCheckBox("", "Disabled Checkbox", "")
	disabledCheck.ID = "defaultCheck2"
	disabledCheck.Disabled = true

	// Render the form
	depends.Render("checkstacked", testForm)
	// Output:
	// <form name="Check Stacked" target="_self" method="post" enctype="application/x-www-form-urlencoded" id="form_check_stacked">
	//     <div class="form-check">
	//         <input type="checkbox" class="form-check-input" id="defaultCheck1">
	//         <label for="defaultCheck1" class="form-check-label">Default Checkbox</label>
	//     </div>
	//     <div class="form-check">
	//         <input type="checkbox" disabled class="form-check-input" id="defaultCheck2">
	//         <label for="defaultCheck2" class="form-check-label">Disabled Checkbox</label>
	//     </div>
	//     <input type="text" name="_method" readonly value="POST" class="form-control" hidden id="form_check_stacked_1.0_text-_method">
	// </form>
}
