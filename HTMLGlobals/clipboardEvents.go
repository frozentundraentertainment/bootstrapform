package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Clipboard Events
//
//
// Attribute	Value
//
// oncopy		script
//
// Fires when the user copies the content of an element
//
//
// oncut		script
//
// Fires when the user cuts the content of an element
//
//
// onpaste		script
//
// Fires when the user pastes some content in an element
type clipboardEvents struct {
	OnCopy  string
	OnCut   string
	OnPaste string
}

func (e *clipboardEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("oncopy", e.OnCopy)
	tag.AddAttributeWithData("oncut", e.OnCut)
	tag.AddAttributeWithData("onpaste", e.OnPaste)
}
