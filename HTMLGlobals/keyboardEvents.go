package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Keyboard Events
//
//
// Attribute	Value
//
// onkeydown	script
//
// Fires when a user is pressing a key
//
//
// onkeypress	script
//
// Fires when a user presses a key
//
//
// onkeyup		script
//
// Fires when a user releases a key
type keyboardEvents struct {
	OnKeyDown  string
	OnKeyPress string
	OnKeyUp    string
}

func (e *keyboardEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("onkeydown", e.OnKeyDown)
	tag.AddAttributeWithData("onkeypress", e.OnKeyPress)
	tag.AddAttributeWithData("onkeyup", e.OnKeyUp)
}
