package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Media Events
//
//
// All are HTML5 Only except for OnAbort
//
//
// Events triggered by medias like videos, images and audio (applies to all HTML
// elements, but is most common in media elements, like <audio>, <embed>, <img>,
// <object>, and <video>).
//
// Tip: Look at our HTML Audio and Video DOM Reference for more information.
//
// See: https://www.w3schools.com/tags/ref_av_dom.asp
//
//
// Attribute		Value
//
// onabort			script
//
// Script to be run on abort
//
//
// oncanplay		script
//
// Script to be run when a file is ready to start playing (when it has buffered
// enough to begin)
//
//
// oncanplaythrough	script
//
// Script to be run when a file can be played all the way to the end without
// pausing for buffering
//
//
// oncuechange		script
//
// Script to be run when the cue changes in a <track> element
//
//
// ondurationchange	script
//
// Script to be run when the length of the media changes
//
//
// onemptied		script
//
// Script to be run when something bad happens and the file is suddenly
// unavailable (like unexpectedly disconnects)
//
//
// onended			script
//
// Script to be run when the media has reach the end (a useful event for messages
// like "thanks for listening")
//
//
// onerror			script
//
// Script to be run when an error occurs when the file is being loaded
//
//
// onloadeddata		script
//
// Script to be run when media data is loaded
//
//
// onloadedmetadata	script
//
// Script to be run when meta data (like dimensions and duration) are loaded
//
//
// onloadstart		script
//
// Script to be run just as the file begins to load before anything is actually
// loaded
//
//
// onpause			script
//
// Script to be run when the media is paused either by the user or programmatically
//
//
// onplay			script
//
// Script to be run when the media is ready to start playing
//
//
// onplaying		script
//
// Script to be run when the media actually has started playing
//
//
// onprogress		script
//
// Script to be run when the browser is in the process of getting the media data
//
//
// onratechange		script
//
// Script to be run each time the playback rate changes (like when a user
// switches to a slow motion or fast forward mode)
//
//
// onseeked			script
//
// Script to be run when the seeking attribute is set to false indicating that
// seeking has ended
//
//
// onseeking		script
//
// Script to be run when the seeking attribute is set to true indicating that
// seeking is active
//
//
// onstalled		script
//
// Script to be run when the browser is unable to fetch the media data for
// whatever reason
//
//
// onsuspend		script
//
// Script to be run when fetching the media data is stopped before it is
// completely loaded for whatever reason
//
//
// ontimeupdate		script
//
// Script to be run when the playing position has changed (like when the user
// fast forwards to a different point in the media)
//
//
// onvolumechange	script
//
// Script to be run each time the volume is changed which (includes setting the
// volume to "mute")
//
//
// onwaiting		script
//
// Script to be run when the media has paused but is expected to resume (like
// when the media pauses to buffer more data)
type mediaEvents struct {
	OnAbort          string
	OnCanPlay        string
	OnCanPlayThrough string
	OnCueChange      string
	OnDurationChange string
	OnEmptied        string
	OnEnded          string
	OnError          string
	OnLoadedData     string
	OnLoadedMetadata string
	OnLoadStart      string
	OnPause          string
	OnPlay           string
	OnPlaying        string
	OnRateChange     string
	OnSeeked         string
	OnSeeking        string
	OnStalled        string
	OnSuspended      string
	OnTimeUpdate     string
	OnVolumeChange   string
	OnWaiting        string
}

func (e *mediaEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("onabort", e.OnAbort)
	tag.AddAttributeWithData("oncanplay", e.OnCanPlay)
	tag.AddAttributeWithData("oncanplaythrough", e.OnCanPlayThrough)
	tag.AddAttributeWithData("oncuechange", e.OnCueChange)
	tag.AddAttributeWithData("ondurationchange", e.OnDurationChange)
	tag.AddAttributeWithData("onemptied", e.OnEmptied)
	tag.AddAttributeWithData("onended", e.OnEnded)
	tag.AddAttributeWithData("onerror", e.OnError)
	tag.AddAttributeWithData("onloadeddata", e.OnLoadedData)
	tag.AddAttributeWithData("onloadedmetadata", e.OnLoadedMetadata)
	tag.AddAttributeWithData("onloadstart", e.OnLoadStart)
	tag.AddAttributeWithData("onpause", e.OnPause)
	tag.AddAttributeWithData("onplay", e.OnPlay)
	tag.AddAttributeWithData("onplaying", e.OnPlaying)
	tag.AddAttributeWithData("onratechange", e.OnRateChange)
	tag.AddAttributeWithData("onseeking", e.OnSeeking)
	tag.AddAttributeWithData("onstalled", e.OnStalled)
	tag.AddAttributeWithData("onsuspended", e.OnSuspended)
	tag.AddAttributeWithData("ontimeupdate", e.OnTimeUpdate)
	tag.AddAttributeWithData("onvoluemchange", e.OnVolumeChange)
	tag.AddAttributeWithData("onwaiting", e.OnWaiting)
}
