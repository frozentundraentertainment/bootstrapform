package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Events
//
// Category			HTML5 Only
//
// Window Events	No
//
// Events triggered for the window object (applies to the <body> tag)
//
//
// Form Events		No
//
// Events triggered by actions inside a HTML form (applies to almost all HTML
// elements, but is most used in form elements)
//
//
// Keyboard Events	No
//
//
// Mouse Events		No
//
//
// Drag Events		Yes
//
//
// Clipboard Events	No
//
//
// Media Events		No
//
// Events triggered by medias like videos, images and audio (applies to all HTML elements, but is most common in media
// elements, like <audio>, <embed>, <img>, <object>, and <video>)
//
// Tip: Look at our HTML Audio and Video DOM Reference for more information.
// https://www.w3schools.com/tags/ref_av_dom.asp
//
//
// Misc Events		Yes
type Events struct {
	windowEvents
	formEvents
	keyboardEvents
	mouseEvents
	dragEvents
	clipboardEvents
	mediaEvents
	miscEvents
}

func (e *Events) Render(tag *html.Tag) {
	e.windowEvents.Render(tag)
	e.formEvents.Render(tag)
	e.keyboardEvents.Render(tag)
	e.mouseEvents.Render(tag)
	e.dragEvents.Render(tag)
	e.clipboardEvents.Render(tag)
	e.mediaEvents.Render(tag)
	e.miscEvents.Render(tag)
}
