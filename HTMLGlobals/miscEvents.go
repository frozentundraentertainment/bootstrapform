package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Misc Events
//
//
// Attribute	Value
//
// ontoggle		script
//
// HTML5 Only
//
// Fires when the user opens or closes the <details> element
type miscEvents struct {
	OnToggle string
}

func (e *miscEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("ontoggle", e.OnToggle)
}
