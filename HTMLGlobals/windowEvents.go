package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Window Events
//
// Events triggered for the window object (applies to the <body> tag):
//
//
// Attribute		Value
//
// onafterprint		script
//
// HTML5 Only
//
// Script to be run after the document is printed
//
//
// onbeforeprint	script
//
// HTML5 Only
//
// Script to be run before the document is printed
//
//
// onbeforeunload	script
//
// Script to be run when the document is about to be unloaded
//
//
// onerror			script
//
// HTML5 Only
//
// Script to be run when an error occurs
//
//
// onhashchange		script
//
// HTML5 Only
//
// Script to be run when there has been changes to the anchor part of the a URL
//
//
// onload			script
//
// Fires after the page is finished loading
//
//
// onmessage		script
//
// HTML5 Only
//
// Script to be run when the message is triggered
//
//
// onoffline		script
//
// HTML5 Only
//
// Script to be run when the browser starts to work offline
//
//
// ononline			script
//
// HTML5 Only
//
// Script to be run when the browser starts to work online
//
//
// onpagehide		script
//
// HTML5 Only
//
// Script to be run when a user navigates away from a page
//
//
// onpageshow		script
//
// HTML5 Only
//
// Script to be run when a user navigates to a page
//
//
// onpopstate		script
//
// HTML5 Only
//
// Script to be run when the window's history changes
//
//
// onresize			script
//
// HTML5 Only
//
// Fires when the browser window is resized
//
//
// onstorage		script
//
// HTML5 Only
//
// Script to be run when a Web Storage area is updated
//
//
// onunload			script
//
// Fires once a page has unloaded (or the browser window has been closed)
type windowEvents struct {
	OnAfterPrint   string
	OnBeforePrint  string
	OnBeforeUnload string
	OnError        string
	OnHashChange   string
	OnLoad         string
	OnMessage      string
	OnOffline      string
	OnOnline       string
	OnPageHide     string
	OnPageShow     string
	OnPopState     string
	OnResize       string
	OnStorage      string
	OnUnload       string
}

func (e *windowEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("onafterprint", e.OnAfterPrint)
	tag.AddAttributeWithData("onbeforeprint", e.OnBeforePrint)
	tag.AddAttributeWithData("onbeforeunload", e.OnBeforeUnload)
	tag.AddAttributeWithData("onerror", e.OnError)
	tag.AddAttributeWithData("onhashchange", e.OnHashChange)
	tag.AddAttributeWithData("onload", e.OnLoad)
	tag.AddAttributeWithData("onmessage", e.OnMessage)
	tag.AddAttributeWithData("onoffline", e.OnOffline)
	tag.AddAttributeWithData("ononline", e.OnOnline)
	tag.AddAttributeWithData("onpagehide", e.OnPageHide)
	tag.AddAttributeWithData("onpageshow", e.OnPageShow)
	tag.AddAttributeWithData("onpopstate", e.OnPopState)
	tag.AddAttributeWithData("onresize", e.OnResize)
	tag.AddAttributeWithData("onstorage", e.OnStorage)
	tag.AddAttributeWithData("onunload", e.OnUnload)
}
