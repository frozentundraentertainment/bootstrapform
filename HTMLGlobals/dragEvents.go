package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Drag Events
//
// HTML5 Only
//
//
// Attribute	Value
//
// ondrag		script
//
// Script to be run when an element is dragged
//
//
// ondragend	script
//
// Script to be run at the end of a drag operation
//
//
// ondragenter	script
//
// Script to be run when an element has been dragged to a valid drop target
//
//
// ondragleave	script
//
// Script to be run when an element leaves a valid drop target
//
//
// ondragover	script
//
// Script to be run when an element is being dragged over a valid drop target
//
//
// ondragstart	script
//
// Script to be run at the start of a drag operation
//
//
// ondrop		script
//
// Script to be run when dragged element is being dropped
//
//
// onscroll		script
//
// Script to be run when an element's scrollbar is being scrolled
type dragEvents struct {
	OnDrag      string
	OnDragEnd   string
	OnDragEnter string
	OnDragLeave string
	OnDragOver  string
	OnDragStart string
	OnDrop      string
	OnScroll    string
}

func (e *dragEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("ondrag", e.OnDrag)
	tag.AddAttributeWithData("ondragend", e.OnDragEnd)
	tag.AddAttributeWithData("ondragenter", e.OnDragEnter)
	tag.AddAttributeWithData("ondragleave", e.OnDragLeave)
	tag.AddAttributeWithData("ondragover", e.OnDragOver)
	tag.AddAttributeWithData("ondragstart", e.OnDragStart)
	tag.AddAttributeWithData("ondrop", e.OnDrop)
	tag.AddAttributeWithData("onscroll", e.OnScroll)
}
