package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Mouse Events
//
//
// Attribute	Value
//
// onclick		script
//
// Fires on a mouse click on the element
//
//
// ondblclick	script
//
// Fires on a mouse double-click on the element
//
//
// onmousedown	script
//
// Fires when a mouse button is pressed down on an element
//
//
// onmousemove	script
//
// Fires when the mouse pointer is moving while it is over an element
//
//
// onmouseout	script
//
// Fires when the mouse pointer moves out of an element
//
//
// onmouseover	script
//
// Fires when the mouse pointer moves over an element
//
//
// onmouseup	script
//
// Fires when a mouse button is released over an element
//
//
// onmousewheel	script
//
// Obsolete. Use the onwheel attribute instead
//
//
// onwheel		script
//
// HTML5 Only
//
// Fires when the mouse wheel rolls up or down over an element
type mouseEvents struct {
	OnClick      string
	OnDblClick   string
	OnMouseDown  string
	OnMouseMove  string
	OnMouseOut   string
	OnMouseOver  string
	OnMouseUp    string
	OnMouseWheel string
	OnWheel      string
}

func (e *mouseEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("onclick", e.OnClick)
	tag.AddAttributeWithData("ondblclick", e.OnDblClick)
	tag.AddAttributeWithData("onmousedown", e.OnMouseDown)
	tag.AddAttributeWithData("onmousemove", e.OnMouseMove)
	tag.AddAttributeWithData("onmouseout", e.OnMouseOut)
	tag.AddAttributeWithData("onmouseover", e.OnMouseOver)
	tag.AddAttributeWithData("onmouseup", e.OnMouseUp)
	tag.AddAttributeWithData("onmousewheel", e.OnMouseWheel)
	tag.AddAttributeWithData("onwheel", e.OnWheel)
}
