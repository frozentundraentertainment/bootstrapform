package HTMLGlobals

import (
	"fmt"
	"strings"

	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

type dropzoneAction string

// DropCopy - Dropping the data will result in a copy of the dragged data
const DropCopy dropzoneAction = "copy"

// DropMove - Dropping the data will result in that the dragged data is moved to the new location
const DropMove dropzoneAction = "move"

// DropLink - Dropping the data will result in a link to the original data
const DropLink dropzoneAction = "link"

// Attributes
//
// Attribute		Description
//
// accesskey		keyboard-letter
//
// Specifies a shortcut key to activate/focus an element
//
//
// class			CSS Classes
//
// Specifies one or more classnames for an element (refers to a class in a style sheet)
//
//
// contenteditable	bool
//
// HTML5 Only
//
// Specifies whether the content of an element is editable or not
//
//
// data-*			customData
//
// HTML5 Only
//
// Used to store custom data private to the page or application
//
//
// dir				text direction
//
// Specifies the text direction for the content in an element
//
//
// draggable		bool
//
// HTML5 Only
//
// Specifies whether an element is draggable or not
//
//
// dropzone			copy
//
// 					move
//
// 					link
//
// HTML5 Only
//
// Specifies whether the dragged data is copied, moved, or linked, when dropped
//
//
// hidden			bool
//
// HTML5 Only
//
// Specifies that an element is not yet, or is no longer, relevant
//
//
// id				string
//
// Specifies a unique id for an element
//
//
// lang				string
//
// Specifies the language of the element's content
//
// See: https://www.w3schools.com/tags/ref_language_codes.asp
//
//
// spellcheck		bool
//
// HTML5 Only
//
// Specifies whether the element is to have its spelling and grammar checked or not
//
// The following can be spellchecked:
//
// 		Text values in input elements (not password)
//
// 		Text in <textarea> elements
//
// 		Text in editable elements
//
//
// style			string
//
// Specifies an inline CSS style for an element
//
//
// tabindex			uint
//
// Specifies the tabbing order of an element
//
//
// title			string
//
// Specifies extra information about an element
//
// The title attribute specifies extra information about an element.
//
// The information is most often shown as a tooltip text when the mouse moves over the element.
//
//
// translate		yes/no
//
// HTML5 Only
//
// Specifies whether the content of an element should be translated or not
//
// Tip: Use class="notranslate" instead.
type Attributes struct {
	AccessKey       string
	class           []string
	ContentEditable bool
	data            map[string]string
	LeftToRight     bool
	Draggable       bool
	DropZone        dropzoneAction
	Hidden          bool
	ID              string
	Name            string
	SpellCheck      bool
	style           map[string]string
	TabIndex        uint
	Title           string
	Translate       bool
	attribute       map[string]string
}

// TODO: Add ARIA attributes

func (a *Attributes) Render(tag *html.Tag) {
	tag.AddAttributeWithData("class", strings.Join(a.class, " "))

	if len(a.style) != 0 {
		var styles []string
		for k, v := range a.style {
			styles = append(styles, fmt.Sprintf("%s=%s;", k, v))
		}
		tag.AddAttributeWithData("style", strings.Join(styles, " "))
	}

	tag.AddAttributeWithData("accesskey", a.AccessKey)
	if a.ContentEditable {
		tag.AddAttribute("contenteditable")
	}
	if a.LeftToRight {
		tag.AddAttributeWithData("dir", "ltr")
	}
	if a.Draggable {
		tag.AddAttributeWithData("draggable", a.Draggable)
	}
	tag.AddAttributeWithData("dropzone", a.DropZone)
	if a.Hidden {
		tag.AddAttribute("hidden")
	}
	tag.AddAttributeWithData("id", a.ID)
	if !a.SpellCheck {
		tag.AddAttributeWithData("spellcheck", a.SpellCheck)
	}
	if a.TabIndex > 0 {
		tag.AddAttributeWithData("tabindex", a.TabIndex)
	}
	tag.AddAttributeWithData("title", a.Title)
	if a.Translate {
		tag.AddAttribute("translate")
	}

	if len(a.data) != 0 {
		for k, v := range a.style {
			tag.AddAttributeWithData("data-"+k, v)
		}
	}

	if len(a.attribute) != 0 {
		for k, v := range a.attribute {
			if v != "" {
				tag.AddAttributeWithData(k, v)
			} else {
				tag.AddAttribute(k)
			}
		}
	}
}

// ClassLookup will return the index and OK bool if a string is found in the slice
func (a *Attributes) ClassLookup(class string) (int, bool) {
	for i, index := range a.class {
		if strings.ToLower(index) == strings.ToLower(class) {
			return i, true
		}
	}
	return 0, false
}

// ClassAdd will add the given class if it is not already associated
func (a *Attributes) ClassAdd(class string) {
	if _, ok := a.ClassLookup(class); !ok {
		a.class = append(a.class, class)
	}
}

// ClassRemove will remove the given class if it is associated
func (a *Attributes) ClassRemove(class string) {
	if i, ok := a.ClassLookup(class); ok {
		a.class = append(a.class[:i], a.class[i+1:]...)
	}
}

// ClassClear will remove all classes
func (a *Attributes) ClassClear() {
	a.class = nil
}

// ClassCount will return the number of classes assigned
func (a *Attributes) ClassCount() int {
	return len(a.class)
}

// DataAdd will add a custom data attribute
func (a *Attributes) DataAdd(tag string, value string) {
	if a.data == nil {
		a.data = make(map[string]string)
	}
	if _, ok := a.data[tag]; !ok {
		a.data[tag] = value
	}
}

// AttributeAdd will add a custom attribute
func (a *Attributes) AttributeAdd(tag string, value string) {
	if a.attribute == nil {
		a.attribute = make(map[string]string)
	}
	if _, ok := a.attribute[tag]; !ok {
		a.attribute[tag] = value
	}
}

// AttributeRemove will remove a custom data attribute
func (a *Attributes) AttributeRemove(tag string) {
	if _, ok := a.attribute[tag]; ok {
		delete(a.attribute, tag)
	}
}

// AttributeClear will remove all custom data attributes
func (a *Attributes) AttributeClear() {
	a.attribute = nil
}

// AttributeCount will return the number of data attributes assigned
func (a *Attributes) AttributeCount() int {
	return len(a.attribute)
}

// DataRemove will remove a custom data attribute
func (a *Attributes) DataRemove(tag string) {
	if _, ok := a.data[tag]; ok {
		delete(a.data, tag)
	}
}

// DataClear will remove all custom data attributes
func (a *Attributes) DataClear() {
	a.data = nil
}

// DataCount will return the number of data attributes assigned
func (a *Attributes) DataCount() int {
	return len(a.data)
}

// StyleAdd will add a custom style attribute
func (a *Attributes) StyleAdd(tag string, value string) {
	if a.style == nil {
		a.style = make(map[string]string)
	}
	if _, ok := a.style[tag]; !ok {
		a.style[tag] = value
	}
}

// StyleRemove will remove a custom style attribute
func (a *Attributes) StyleRemove(tag string) {
	if _, ok := a.style[tag]; ok {
		delete(a.style, tag)
	}
}

// StyleClear will remove all styles
func (a *Attributes) StyleClear() {
	a.style = nil
}

// StyleCount will return the number of styles assigned
func (a *Attributes) StyleCount() int {
	return len(a.style)
}
