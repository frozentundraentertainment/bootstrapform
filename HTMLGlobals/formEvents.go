package HTMLGlobals

import (
	"gitlab.com/frozentundraentertainment/bootstrapform/html"
)

// Form Events
//
// Events triggered by actions inside a HTML form (applies to almost all HTML elements, but is most used in form elements):
//
//
// Attribute		Value
//
// onblur			script
//
// Fires the moment that the element loses focus
//
//
// onchange			script
//
// Fires the moment when the value of the element is changed
//
//
// oncontextmenu	script
//
// HTML5 Only
//
// Script to be run when a context menu is triggered
//
//
// onfocus			script
//
// Fires the moment when the element gets focus
//
//
// oninput			script
//
// HTML5 Only
//
// Script to be run when an element gets user input
//
//
// oninvalid		script
//
// HTML5 Only
//
// Script to be run when an element is invalid
//
//
// onreset			script
//
// HTML5 Only
//
// Fires when the Reset button in a form is clicked
//
//
// onsearch			script
//
// Fires when the user writes something in a search field (for <input="search">)
//
//
// onselect			script
//
// Fires after some text has been selected in an element
//
//
// onsubmit			script
//
// Fires when a form is submitted
type formEvents struct {
	OnBlur        string
	OnChange      string
	OnContextMenu string
	OnFocus       string
	OnInput       string
	OnInvalid     string
	OnReset       string
	OnSearch      string
	OnSelect      string
	OnSubmit      string
}

func (e *formEvents) Render(tag *html.Tag) {
	tag.AddAttributeWithData("onblur", e.OnBlur)
	tag.AddAttributeWithData("onchange", e.OnChange)
	tag.AddAttributeWithData("oncontextmenu", e.OnContextMenu)
	tag.AddAttributeWithData("onfocus", e.OnFocus)
	tag.AddAttributeWithData("oninput", e.OnInput)
	tag.AddAttributeWithData("oninvalid", e.OnInvalid)
	tag.AddAttributeWithData("onreset", e.OnReset)
	tag.AddAttributeWithData("onsearch", e.OnSearch)
	tag.AddAttributeWithData("onselect", e.OnSelect)
	tag.AddAttributeWithData("onsubmit", e.OnSubmit)
}
