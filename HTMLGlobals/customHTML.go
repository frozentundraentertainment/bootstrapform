package HTMLGlobals

// CustomHTML will hold user provided HTML before and after the open and close
// tag of each item it is assigned to
type CustomHTML struct {
	BeforeOpenTag string
	AfterOpenTag  string
	BeforeEndTag  string
	AfterEndTag   string
}
